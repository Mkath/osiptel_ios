//
//  AppDelegate.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 29/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Lottie
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import KeychainSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var timer: Timer!
    var imageview: UIImageView!
    weak var screen : UIView? = nil // property of the AppDelegate

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.        
        DataService.shared.requestToken()
     
       /*
        }*/
        
        NotificationCenter.default.addObserver(self, selector: #selector(postTokenAcquisitionScript), name: NSNotification.Name(rawValue: "tokenAcquired"), object: nil)
        
        GMSServices.provideAPIKey("AIzaSyCHtBxrwuGGg5432EobYIDPD2z-ab1cZAc")
        GMSPlacesClient.provideAPIKey("AIzaSyCHtBxrwuGGg5432EobYIDPD2z-ab1cZAc")

        MSAppCenter.start("a54d60e9-6038-4510-89a4-8663dcaebf0c", withServices:[ MSAnalytics.self, MSCrashes.self ])
        
        NotificationCenter.default.addObserver(self, selector: #selector(test), name: .UIApplicationUserDidTakeScreenshot, object: nil)

        
        let modelName = UIDevice.current.modelName
        var colors = [UIColor]()
        colors.append(UIColor(red:0.13, green:0.71, blue:0.85, alpha:1.0))
        colors.append(UIColor(red:0.11, green:0.44, blue:0.69, alpha:1.0))
        
        var navigateAppearace = UINavigationBar.appearance()
        navigateAppearace.barTintColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
        navigateAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        var idVendor = UIDevice.current.identifierForVendor
        var UUID = UIDevice.current.identifierForVendor?.uuidString
        
        let keychain = KeychainSwift()
        if keychain.get("UUIDkey") == nil || keychain.get("UUIDkey") == ""{
            keychain.set(UUID!, forKey: "UUIDkey")
            UserDefaults.standard.setUUID(value: UUID!)
        }else{
            UserDefaults.standard.setUUID(value: keychain.get("UUIDkey")!)
        }

        return true
    }
    
    @objc func test() {
        //imageview = UIImageView.init(image: UIImage.init(named: "lauch_screen"))
        //self.window?.addSubview(imageview!)
    }
    
    @objc func postTokenAcquisitionScript() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
    }
    
    @objc func tick() {
        if let time = UserDefaults.standard.value(forKey: "tokenAcquisitionTime") as? Date {
            if Date().timeIntervalSince(time) > 3600 { //You can change '3600' to your desired value. Keep in mind that this value is in seconds. So in this case, it is checking for an hour
                timer.invalidate()
                DataService.shared.requestToken()
               /*DataService.shared.requestToken{ [weak self] (error) in
                    if let error = error {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NotConnectionViewController") as! NotConnectionViewController
                        vc.modalPresentationStyle = .overFullScreen
                        
                        //self.present(vc, animated: false, completion: nil)
                        
                    }
                }*/
            }
        }
    }

    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    static func showAlertView(vc : UIViewController, titleString : String , messageString: String) ->()
    {
        let alertView = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Aceptar", style: .cancel) { (alert) in
            //vc.dismiss(animated: true, completion: nil)
        }
        alertView.addAction(alertAction)
        vc.present(alertView, animated: true, completion: nil)     
    }

    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        imageview = UIImageView.init(image: UIImage.init(named: "lauch_screen"))
        self.window?.addSubview(imageview!)
        
        
        if UserDefaults.standard.getSesionTime(){
            let date = NSDate()
            let resignTime = Int64(date.timeIntervalSince1970)
            UserDefaults.standard.set(resignTime, forKey: "logOutTime")
        }

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "tokenAcquired"), object: nil)

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.addObserver(self, selector: #selector(postTokenAcquisitionScript), name: NSNotification.Name(rawValue: "tokenAcquired"), object: nil)

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if UserDefaults.standard.getSesionTime(){
            if let resignTime = UserDefaults.standard.object(forKey: "logOutTime") as? Int64 {
                let date = NSDate()
                let currentTime = Int64(date.timeIntervalSince1970)
                debugPrint("diff: ", currentTime-resignTime)
                let diff = currentTime-resignTime
                 let tiempoTrasu = UserDefaults.standard.getTrasuTiempo() / 20
                //
                if diff >= tiempoTrasu{ //checking difference of time which you expect
                    UserDefaults.standard.setSesionTime(value: false)
                    NotificationCenter.default.post(name: Notification.Name.session, object: nil)
                }
            }
        }
        
        if (imageview != nil){            
            imageview?.removeFromSuperview()
            imageview = nil
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }   

}

extension Notification.Name{
    static let session = Notification.Name(rawValue: "session")
}

extension UIApplication {
    
    var screenShot: UIImage?  {
        
        if let layer = keyWindow?.layer {
            let scale = UIScreen.main.scale
            
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            if let context = UIGraphicsGetCurrentContext() {
                layer.render(in: context)
                let screenshot = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                return screenshot
            }
        }
        return nil
    }
}
