//
//  DocumentFormatViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/22/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DocumentFormatViewController: UIViewController,NVActivityIndicatorViewable {

    var _plantilla = Plantilla()
    @IBOutlet weak var tblDocument: UITableView!
    
    var _formato = ["Reclamo telefonía fija","Reclamo telefonía móvil","Reclamo de otros servicios publicos de telecomunicaciones","Apelaciones","Quejas"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        self.title = "Formularios"
        attempFetchPregunta()
    }
    
    func attempFetchPregunta(){
        DataService.shared.requestFetchPlantilla{ [weak self] (plantilla, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                self!.stopAnimating()
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let plantilla = plantilla else{
                return
            }
            self?.updatePlantillas(with: plantilla)
        }
    }
    
    func updatePlantillas(with plantilla: Plantilla) {
        
        _plantilla = plantilla
        tblDocument.reloadData()
        stopAnimating()
    }
    
}
extension DocumentFormatViewController: UITableViewDelegate, UITableViewDataSource, DownloadFileDelegate{
    func DescargarPdf(descarga: String, nombre: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PdfWebViewViewController") as! PdfWebViewViewController
    
        controller.urlExternal = descarga
        controller.titleFile = nombre
        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func DescargarWord(descarga: String, nombre: String) {

        if descarga != ""{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "PdfWebViewViewController") as! PdfWebViewViewController
            
            controller.urlExternal = descarga
            controller.titleFile = nombre
            
            let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem = item
            navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            //alertViewController(titulo: "", mensaje: "El formato no se encuentra disponible", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            AppDelegate.showAlertView(vc: self, titleString: "", messageString: "El formato no se encuentra disponible")
        }
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _plantilla.count + 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 250
            
        }else {
            return 55
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row > 0{
            let plantillas = _plantilla[indexPath.row - 1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "downloadCell", for: indexPath) as! DownloadFileTableViewCell            
            cell.setDownlaod(pregunta: plantillas)
            cell.delegate = self
            cell.lblDownload.text = _formato[indexPath.row - 1]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "informationCell", for: indexPath) as! InformationDownloadTableViewCell
            return cell
        }
    }

    
}


