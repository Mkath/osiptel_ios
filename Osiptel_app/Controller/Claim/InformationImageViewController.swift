//
//  InformationImageViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/15/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView

class InformationImageViewController: UIViewController, UIScrollViewDelegate,NVActivityIndicatorViewable{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var imagenInformation : String = ""
    
    var frame  = CGRect(x:0, y:0, width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("image: \(imagenInformation)")
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        let original =  imagenInformation
        let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        imageView.sd_setImage(with: URL(string: encoded ?? ""), placeholderImage: nil, options: []) { (image, error, imageCacheType, imageUrl) in
            self.stopAnimating()
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
   
}



