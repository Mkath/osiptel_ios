//
//  InformationViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class InformationViewController: UIViewController{

    var _reclamo = Reclamo()
    
    @IBOutlet weak var informationView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1596710384, green: 0.7130240202, blue: 0.8868247867, alpha: 1)

        navigationItem.title = "Guía de información"
        
        let item = UIBarButtonItem(image: UIImage(named: "ic_information_home"),
            style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = item
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
            UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        attempFetchReclamos()
        
    }

    
    func attempFetchReclamos(){
        DataService.shared.requestFetchReclamo{ [weak self] (reclamo, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                self!.stopAnimating()
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let reclamo = reclamo else{
                return
            }
            self?.updateReclamos(with: reclamo)
        }
    }
    
    func updateReclamos(with reclamo: Reclamo) {
        
        _reclamo = reclamo
        print("Reclamo \(_reclamo)")
        informationView.reloadData()
        stopAnimating()
        
    }
}

extension InformationViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NVActivityIndicatorViewable{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _reclamo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InformationCell", for: indexPath) as! InformationCollectionViewCell        
        cell.lblInformation.text = _reclamo[indexPath.row].descripcion
        cell.imageInformation.image = UIImage(named: "ic_reclamo_\(indexPath.row + 1)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 35.0
        layout.minimumInteritemSpacing = 2.5
        
        let numberOfItemsPerRow: CGFloat = 2.0
        let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing) / numberOfItemsPerRow
        print("itemwidth")
        print(itemWidth)
        return CGSize(width: itemWidth, height: itemWidth/1.3)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InformationImageViewController") as! InformationImageViewController
        let opcionesPosicion = _reclamo[indexPath.row]
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1596710384, green: 0.7130240202, blue: 0.8868247867, alpha: 1)
        
        navigationItem.title = "Guía de información"
        
        let item = UIBarButtonItem(image: UIImage(named: "ic_information_home"),
            style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = item
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        controller.navigationItem.title = _reclamo[indexPath.row].descripcion as? String
        
        if indexPath.row == _reclamo.count - 1{
              let controller = storyboard.instantiateViewController(withIdentifier: "DocumentFormatViewController") as! DocumentFormatViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }else if _reclamo.count > indexPath.row{
            let imagenURL = _reclamo[indexPath.row].nombreImagen
            if imagenURL != nil {
                controller.imagenInformation = imagenURL as! String
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}

