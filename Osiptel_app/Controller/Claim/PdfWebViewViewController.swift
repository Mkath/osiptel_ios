//
//  PdfWebViewViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/28/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import NVActivityIndicatorView

class PdfWebViewViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var webView: UIView!
    private var WkwebView: WKWebView!
    var urlExternal: String = ""
    var titleFile : String = ""
    var nombreExpediente : String = ""
    var downloadFlag = false
    
    
    @IBOutlet weak var messageView: UIView!
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        let url = NSURL(fileURLWithPath: urlExternal)
        
        if let pdfData = NSData(contentsOf: url as URL) {
            
            let resourceDocPath = NSHomeDirectory().appending("/Documents/yourPDF.pdf")
            
            unlink(resourceDocPath)
            
            pdfData.write(toFile: resourceDocPath, atomically: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.load(url: self.urlExternal)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = titleFile 
        let webViewPreferences = WKPreferences()
        webViewPreferences.javaScriptEnabled = true
        webViewPreferences.javaScriptCanOpenWindowsAutomatically = true
        
        let webViewConf = WKWebViewConfiguration()
        webViewConf.preferences = webViewPreferences
        WkwebView = WKWebView(frame: webView.frame, configuration: webViewConf)
        WkwebView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.addSubview(WkwebView)

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            print("***documentURL: ",documentsURL)
            let fileArray = self.urlExternal.components(separatedBy: "/")
            let finalFileName = fileArray.last
            print(finalFileName)
            let fileURL = documentsURL.appendingPathComponent("\(finalFileName!)")
            print("***fileURL: ",fileURL ?? "")
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        
        Alamofire.download(urlExternal, to: destination).response { response in
            print(response)
            
            if response.error == nil, let imagePath = response.destinationURL?.path {
                let filePathURL = NSURL(fileURLWithPath: imagePath)
                print("filePathURL \(filePathURL)")
                
                self.WkwebView.load(URLRequest(url: filePathURL as URL))
                
                let fileManager = FileManager.default
                let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                
                var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                let fileArray = self.urlExternal.components(separatedBy: "/")
                let finalFileName = fileArray.last
                pdfURL = pdfURL.appendingPathComponent("\(finalFileName!)") as URL
                let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
                print("finalFileName")
                print(finalFileName)
                let destinationFileUrl = documentsUrl.appendingPathComponent("\(finalFileName!)") as URL
                

                let fileURL = URL(string: self.urlExternal)
                
                let sessionConfig = URLSessionConfiguration.default
                let session = URLSession(configuration: sessionConfig)
                
                let request = URLRequest(url:fileURL!)
                
                let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                    if let tempLocalUrl = tempLocalUrl, error == nil {
                        // Success
                        if ((response as? HTTPURLResponse)?.statusCode) != nil {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                                        self.messageView.alpha = 0
                            }
                        }
                        
                        do {
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        } catch (let writeError) {
                            print("Error creating a file \(destinationFileUrl) : \(writeError)")
                        }
                        
                    } else {
                        print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any);
                    }
                }
                task.resume()
                 NVActivityIndicatorPresenter.sharedInstance.setMessage("Abriendo plantilla...")
                self.load(url: self.urlExternal)
                self.messageView.alpha = 1
                self.stopAnimating()
            }
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.messageView.alpha = 1
        })
        
    }
    
    
    
    private func load(url: String){
        WkwebView.load(URLRequest(url: URL(string: url)!))
    }
}


extension PdfWebViewViewController: URLSessionDownloadDelegate{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            //self.pdfURL = destinationURL
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
