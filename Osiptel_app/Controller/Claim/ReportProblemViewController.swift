//
//  Report problem ReportProblemViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/14/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ReportProblemViewController: UIViewController,NVActivityIndicatorViewable,selectedProblemDelegate {
    func sendProblem(idproblema: String ,problema: String) {
        idProblema = Int(idproblema)!
        txtProblemaDetectado.text = problema
    }
    

    @IBOutlet weak var txtCodigoIMEI: UITextField!
    @IBOutlet weak var txtEmpresa: UITextField!
    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtPaterno: UITextField!
    @IBOutlet weak var txtMaterno: UITextField!
    @IBOutlet weak var txtMarca: UITextField!
    @IBOutlet weak var txtModelo: UITextField!
    @IBOutlet weak var txtNumeroMovil: UITextField!
    @IBOutlet weak var txtCodigoBloqueo: UITextField!
    @IBOutlet weak var txtFechaReporteOperador: UITextField!
    @IBOutlet weak var txtFechaReporteContacto: UITextField!
    @IBOutlet weak var txtNumeroContacto: UITextField!
    @IBOutlet weak var txtProblemaDetectado: UITextField!
    @IBOutlet weak var scrollReport: UIScrollView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblMensajeImei: UILabel!
    
    var problemSelected = ProblemSelectedTableViewController()
    
    var _empresa = Operador()
    var _empresaElement : OperadorElement?
    var _problemaElement : ProblemaElement?
    var _marcaElement : MarcaElement?
    var _modeloElement : ModeloElement?
    var _marca = Marca()
    var _modelo = Modelo()
    var _problema = Problema()
    var isValidate = false
    
    
    var idEmpresa: String = "0"
    var idMarca: Int = 0
    var idModelo: Int = 0
    var idProblema: Int = 0
    
    private var Pickerempresa : UIPickerView?
    private var Pickermarca : UIPickerView?
    private var Pickermodelo : UIPickerView?
    private var PickerproblemaDetectado : UIPickerView?
    private var fechaReporteOperador: UIDatePicker?
    private var fechaReporteContacto: UIDatePicker?
    let myColor = UIColor.red
    var imei : String = ""
    
    
    @IBAction func btnEnviarAction(_ sender: Any) {
    
        var isValidate = true
        
        if txtCodigoIMEI.text!.count == 0{
            txtCodigoIMEI.borderStyle = UITextBorderStyle.roundedRect
            txtCodigoIMEI.layer.borderColor = myColor.cgColor
            txtCodigoIMEI.layer.cornerRadius = 4
            txtCodigoIMEI.borderColorView = UIColor.red
            txtCodigoIMEI.layer.borderWidth = 1.0
            lblMensajeImei.isHidden = true
            isValidate = false
        }
        
        if txtCodigoIMEI.text!.count > 0 && txtCodigoIMEI.text!.count < 15 {
            txtCodigoIMEI.borderStyle = UITextBorderStyle.roundedRect
            txtCodigoIMEI.layer.borderColor = myColor.cgColor
            txtCodigoIMEI.layer.cornerRadius = 4
            txtCodigoIMEI.borderColorView = UIColor.red
            txtCodigoIMEI.layer.borderWidth = 1.0
            lblMensajeImei.isHidden = false
            isValidate = false
        }
        
        if txtEmpresa.text!.count == 0 || txtEmpresa.text == "Seleccione"{
            txtEmpresa.borderStyle = UITextBorderStyle.roundedRect
            txtEmpresa.layer.borderColor = myColor.cgColor
            txtEmpresa.layer.cornerRadius = 4
            txtEmpresa.layer.cornerRadius = 4
            txtEmpresa.borderColorView = UIColor.red
            txtEmpresa.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if (txtNombres.text?.isEmpty)!{
            txtNombres.borderStyle = UITextBorderStyle.roundedRect
            txtNombres.layer.borderColor = myColor.cgColor
            txtNombres.layer.cornerRadius = 4
            txtNombres.layer.cornerRadius = 4
            txtNombres.borderColorView = UIColor.red
            txtNombres.layer.borderWidth = 1.0
            isValidate = false
        }

        if (txtPaterno.text?.isEmpty)!{
            txtPaterno.borderStyle = UITextBorderStyle.roundedRect
            txtPaterno.layer.borderColor = myColor.cgColor
            txtPaterno.layer.cornerRadius = 4
            txtPaterno.layer.cornerRadius = 4
            txtPaterno.borderColorView = UIColor.red
            txtPaterno.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if (txtMaterno.text?.isEmpty)!{
            txtMaterno.borderStyle = UITextBorderStyle.roundedRect
            txtMaterno.layer.borderColor = myColor.cgColor
            txtMaterno.layer.cornerRadius = 4
            txtMaterno.layer.cornerRadius = 4
            txtMaterno.borderColorView = UIColor.red
            txtMaterno.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if txtMarca.text!.count == 0 || txtMarca.text == "Seleccione"{
            txtMarca.borderStyle = UITextBorderStyle.roundedRect
            txtMarca.layer.borderColor = myColor.cgColor
            txtMarca.layer.cornerRadius = 4
            txtMarca.layer.cornerRadius = 4
            txtMarca.borderColorView = UIColor.red
            txtMarca.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if txtNumeroMovil.text!.count < 7{
            txtNumeroMovil.borderStyle = UITextBorderStyle.roundedRect
            txtNumeroMovil.layer.borderColor = myColor.cgColor
            txtNumeroMovil.layer.cornerRadius = 4
            txtNumeroMovil.layer.cornerRadius = 4
            txtNumeroMovil.borderColorView = UIColor.red
            txtNumeroMovil.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if txtProblemaDetectado.text!.count == 0 || txtProblemaDetectado.text == "Seleccione"{
            txtProblemaDetectado.borderStyle = UITextBorderStyle.roundedRect
            txtProblemaDetectado.layer.borderColor = myColor.cgColor
            txtProblemaDetectado.layer.cornerRadius = 4
            txtProblemaDetectado.layer.cornerRadius = 4
            txtProblemaDetectado.borderColorView = UIColor.red
            txtProblemaDetectado.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if (txtEmail.text!.isEmpty){
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = myColor.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            isValidate = false
        }
        

        if !isValidEmailAddress(emailAddressString: txtEmail.text!){
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = myColor.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            isValidate = false
        }

        if isValidate{
            lblMensajeImei.isHidden = true
            self.attempFetchReportPost()
        }else{
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
            UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        
        navigationController?.navigationBar.tintColor = .white
        
        //NoticacionCenter Movimiento de vista con respecto al teclado
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)

        problemSelected.delegate = self
        
        //dismissKeyboard()
        txtCodigoIMEI.delegate = self
        txtCodigoIMEI.tag = 0
        txtEmpresa.delegate = self
        txtNombres.delegate = self
        txtNombres.tag = 2
        txtPaterno.delegate = self
        txtPaterno.tag = 3
        txtMaterno.delegate = self
        txtMaterno.tag = 4
        txtMarca.delegate = self
        txtModelo.delegate = self
        txtNumeroMovil.delegate = self
        txtCodigoBloqueo.delegate = self
        txtCodigoBloqueo.tag = 8
        txtFechaReporteOperador.delegate = self
        txtFechaReporteOperador.tag = 9
        txtFechaReporteContacto.delegate = self
        txtNumeroContacto.delegate = self
        txtProblemaDetectado.delegate = self
        txtNumeroContacto.tag = 12
        txtEmail.delegate = self
        txtEmail.tag = 13
        Pickerempresa = UIPickerView()
        Pickermarca = UIPickerView()
        Pickermodelo = UIPickerView()
        Pickermarca = UIPickerView()
        PickerproblemaDetectado = UIPickerView()
        Pickerempresa?.delegate = self
        Pickerempresa?.dataSource = self
        Pickerempresa?.tag = 1
        txtEmpresa.inputView = Pickerempresa
        Pickermarca?.delegate = self
        Pickermarca?.dataSource = self
        Pickermarca?.tag = 5
        txtMarca.inputView = Pickermarca
        txtNumeroMovil.tag = 7
        txtNumeroContacto.tag = 11
        txtEmpresa.tag = 12
        Pickermodelo?.delegate = self
        Pickermodelo?.dataSource = self
        Pickermodelo?.tag = 6
        txtModelo.inputView = Pickermodelo
        txtModelo.text = "Seleccione"
        txtModelo.isEnabled = false
        txtProblemaDetectado.tag = 20
        
        let tapGesturetex = UITapGestureRecognizer(target: self, action: #selector(tapOnTextView(_:)))
        txtProblemaDetectado.addGestureRecognizer(tapGesturetex)
        fechaReporteOperador = UIDatePicker()
        fechaReporteOperador?.datePickerMode = .dateAndTime
        fechaReporteOperador?.maximumDate = Date()
        fechaReporteOperador?.tag = 10
        
        fechaReporteContacto = UIDatePicker()
        fechaReporteContacto?.datePickerMode = .date
        fechaReporteContacto?.maximumDate = Date()
        fechaReporteContacto?.tag = 11
        fechaReporteContacto?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        fechaReporteOperador?.addTarget(self, action: #selector(dateTimeChanged(datePicker:)), for: .valueChanged)
        txtFechaReporteOperador.inputView = fechaReporteOperador
        txtFechaReporteContacto.inputView = fechaReporteContacto
        txtCodigoIMEI.text = imei
        txtNombres.text = UserDefaults.standard.getName()
        txtPaterno.text = UserDefaults.standard.getLastnameP()
        txtMaterno.text = UserDefaults.standard.getLastnameM()
        txtNumeroMovil.text = UserDefaults.standard.getNumber()
        txtEmail.text = UserDefaults.standard.getEmail()
        lblMensajeImei.isHidden = true
        txtEmpresa.text = "Seleccione"
        txtMarca.text = "Seleccione"
        txtProblemaDetectado.text = "Seleccione"
        attempFetchMarca()
        attempFetchEmpresa()
        attempFetchProblema()
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.dateFormat = "dd/MM/YYYY HH:mm"
        txtFechaReporteOperador.text = formatter.string(from: Date())

    }

    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    @objc private final func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        self.view.endEditing(true)
        txtProblemaDetectado.layer.borderWidth = 0
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ProblemSelectedTableViewController") as! ProblemSelectedTableViewController
        let backItem = UIBarButtonItem()
        vc.delegate = self
        backItem.title = " "
        vc._problema = _problema
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func dateChanged(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        txtFechaReporteContacto.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    @objc func dateTimeChanged(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY HH:mm"
        txtFechaReporteOperador.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
    //Services
    func attempFetchMarca(){
        DataService.shared.requestFetchMarca{ [weak self] (marca, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let marca = marca else{
                return
            }
            self?.updateMarca(with: marca)
        }
    }
    
    func updateMarca(with marca: Marca) {
        _marca = marca
        _marca.insert(MarcaElement.init(idMarca: 0 ,descripcion: "Seleccionar"), at: 0)
        Pickermarca?.reloadAllComponents()
    }
    
    
    func attempFetchEmpresa(){
        DataService.shared.requestFetchOperadores{ [weak self] (empresa, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let empresa = empresa else{
                return
            }
            self?.updateEmpresa(with: empresa)
        }
    }
    
    func updateEmpresa(with empresa: Operador) {
        _empresa = empresa
        _empresa.insert(OperadorElement.init(idOperador: 0, nombreComercial: "Seleccione", razonSocial: "", enlace: "", nombreImagen: "", codigoOperador: ""), at: 0)
        
        Pickerempresa?.reloadAllComponents()
        stopAnimating()
    }
    
    func attempFetchProblema(){
        DataService.shared.requestFetchProblema{ [weak self] (problema, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let problema = problema else{
                return
            }
            self?.updateProblema(with: problema)
        }
    }
    
    func updateProblema(with problema: Problema) {
        _problema = problema
    }
    
    func attempFetchModelo(marca:Int){
        DataService.shared.requestFetchModeloByMarca(with: marca){ [weak self] (modelo, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let modelo = modelo else{
                return
            }
            self?.updateModelo(with: modelo)
        }
    }
    
    func updateModelo(with modelo: Modelo) {
        _modelo = modelo
        txtModelo.isEnabled = true
        txtModelo.text = "Seleccione"
        _modelo.insert(ModeloElement.init(idModelo: 0, idMarca: 0, descripcion: "Seleccionar"), at: 0)
        Pickermodelo?.reloadAllComponents()
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    ///Registro Problema
    func attempFetchReportPost(){
        
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
            UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        let _postImei = PostImei.init(codigoImei: txtCodigoIMEI.text!, empresaOperadora: Int(idEmpresa)!, descripcionEmpresaOperadora: txtEmpresa.text!, nombres: txtNombres.text!, apellidoPaterno: txtPaterno.text!, apellidoMaterno: txtMaterno.text!, idMarca: idMarca, descripcionMarca: txtMarca.text!, idModelo: idModelo, descripcionModelo: txtModelo.text!, numeroMovil: txtNumeroMovil.text!, codigoBloqueo: txtCodigoBloqueo.text!, fechaReporte: txtFechaReporteOperador.text!, horaReporte: txtFechaReporteOperador.text!, minutoReporte: txtFechaReporteOperador.text!, fechaRecuperacion: txtFechaReporteContacto.text!, numeroContacto: txtNumeroContacto.text!, problemaDetectado: idProblema, correoElectronico: txtEmail.text!, guid: UserDefaults.standard.getUUID(), comentario: "", archivo: "")

        DataService.shared.requestFetchCreateProblemImei(with: _postImei) {[weak self] (imei, error,status,mensaje)  in
            if let error = error {
                print("imeiPost \(error)")
                self!.alertViewController(titulo: "", mensaje: "No se pudo enviar el reporte", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                return
            }
            if status == "200"{
                self!.updateImeiPost()
            }else if status == "400"{
                self!.stopAnimating()
                self!.alertViewController(titulo: "", mensaje: mensaje, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "Imei")
            }
            
        }
    }

    func updateImeiPost(){
        stopAnimating()
        self.alertViewController(titulo: "", mensaje: "Su registro fue enviado con éxito", closeFlag: true, imageValidate: true, closeAutomatic: false, sendVC: "Imei")
    }
}

extension ReportProblemViewController: UIScrollViewDelegate{
    
    @objc func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollReport.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollReport.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollReport.contentInset = contentInset
    }
}


extension ReportProblemViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if  pickerView.tag == 1{
            return _empresa.count
        }
        
        if  pickerView.tag  == 5{
            return _marca.count
        }
        
        if  pickerView.tag  == 6{
            return _modelo.count
        }
        
        if  pickerView.tag == 20{
            return _problema.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
        
        if pickerView.tag == 1{
            txtEmpresa.layer.borderWidth = 0
            if _empresa.count != 0{
                idEmpresa = _empresa[row].codigoOperador
                txtEmpresa.text = _empresa[row].nombreComercial
            }
        }
        
        if pickerView.tag == 5{
            txtMarca.layer.borderWidth = 0
            if _marca.count != 0{
                txtMarca.text = _marca[row].descripcion
                txtModelo.text = ""
                idMarca = _marca[row].idMarca
                attempFetchModelo(marca: idMarca)
            }
        }
        
        if pickerView.tag == 6{
            txtModelo.layer.borderWidth = 0
            if _modelo.count != 0{
                idModelo = _modelo[row].idModelo
                txtModelo.text = _modelo[row].descripcion
            }
        }
        
        if pickerView.tag == 9 {
            txtFechaReporteOperador.layer.borderWidth = 0
        }

        if pickerView.tag == 10 {
            txtFechaReporteContacto.layer.borderWidth = 0
        }
        
        if pickerView.tag == 20{
            txtProblemaDetectado.layer.borderWidth = 0
        }
        

    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if  pickerView.tag == 1{
            if _empresa.count != 0{
                return _empresa[row].nombreComercial
            }else{
                return ""
            }
        }
        
        if  pickerView.tag == 5{
            if _marca.count != 0{
                txtModelo.isEnabled = false
                txtModelo.text = "Seleccione"
                return _marca[row].descripcion
            }else{
                return ""
            }
        }
        
        if  pickerView.tag == 6{
            if _modelo.count != 0{
                return _modelo[row].descripcion
            }else{
                return ""
            }

        }
        
        if  pickerView.tag == 12{
            if _problema.count != 0{
                return _problema[row].descripcion
            }else{
                return ""
            }            
        }
        return ""
    }
    
    
}

extension ReportProblemViewController: UITextFieldDelegate{
 
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //-'a-zA-ZÀ-ÖØ-öø-ÿ]+
        
        let regex = try! NSRegularExpression(pattern: "[a-zäáàëéèíìöóòúùÑñA-Z\\s]+", options: [])

        let regexAlpha = try! NSRegularExpression(pattern: "[a-zA-Z0-9\\s]+", options: [])
        
        if textField.tag == 0{
            txtCodigoIMEI.layer.borderWidth = 0
            lblMensajeImei.isHidden = true
        }
      
        
        if textField.tag == 0{
            let count: Int = textField.text?.count ?? 0

            if count > 14{
                lblMensajeImei.alpha = 0
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace: Int = Int(strcmp(char, "\u{8}"))
                if isBackSpace != -8 {
                    dismissKeyboard()
                }
            }
        }
        
        if textField.tag == 0{
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        

        if textField.tag == 1{
            txtEmpresa.layer.borderWidth = 0
        }
        if textField.tag == 2{
            txtNombres.layer.borderWidth = 0
        }
        if textField.tag == 3{
            txtPaterno.layer.borderWidth = 0
        }
        if textField.tag == 4{
            txtMaterno.layer.borderWidth = 0
        }
        if textField.tag == 5{
            txtMarca.layer.borderWidth = 0
        }
        if textField.tag == 6{
            txtModelo.layer.borderWidth = 0
        }
        if textField.tag == 7{
            txtNumeroMovil.layer.borderWidth = 0
            let count: Int = textField.text?.count ?? 0
            if count > 8{
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace: Int = Int(strcmp(char, "\u{8}"))
                if isBackSpace != -8 {
                    dismissKeyboard()
                }
            }
        }
        if textField.tag == 8{
            txtCodigoBloqueo.layer.borderWidth = 0
        }
        if textField.tag == 9{
            txtFechaReporteOperador.layer.borderWidth = 0
        }
        if textField.tag == 10{
            txtFechaReporteContacto.layer.borderWidth = 0
        }
        if textField.tag == 11{
            txtNumeroContacto.layer.borderWidth = 0
            let count: Int = textField.text?.count ?? 0
            if count > 8{
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace: Int = Int(strcmp(char, "\u{8}"))
                if isBackSpace != -8 {
                    dismissKeyboard()
                }
            }
        }
        if textField.tag == 20{
            txtProblemaDetectado.layer.borderWidth = 0
        }
        if textField.tag == 13{
            txtEmail.layer.borderWidth = 0
        }
        
        //Solo letras
        if textField.tag == 2 || textField.tag == 3 || textField.tag == 4{
            let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.characters.count))
            return range.length == string.characters.count
        }
        
        //Solo letras
        if textField.tag == 8 {
            let range = regexAlpha.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.characters.count))
            return range.length == string.characters.count
        }
        
        //Limite de caracteres
        if textField.tag == 1{
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 7{
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength            
        }
        
        if textField.tag == 7{
            guard NSCharacterSet(charactersIn: "0123456789").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            return true
        }
        
        if textField.tag == 11{
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 11{
            guard NSCharacterSet(charactersIn: "0123456789").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            return true
        }
        return  true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
