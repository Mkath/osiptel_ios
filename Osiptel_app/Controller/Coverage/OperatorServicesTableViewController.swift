//
//  OperatorServicesTableViewController.swift
//  Osiptel_app
//
//  Created by GYS on 3/26/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol parseServiceDelegate : class {
    func SendAllServiceSelected(tipoServicio : [String], idTipoServicio: [Int], send: Bool)
}

class OperatorServicesTableViewController: UITableViewController,OperatorServicesTableViewCellDelegate, servicesSendDelegate, NVActivityIndicatorViewable {
    func SenderServiceTableViewCellDidLocalidad() {
        
        if TipoServicioSelect.count > 0{
            delegate?.SendAllServiceSelected(tipoServicio: TipoServicioSelect, idTipoServicio: idTipoServicioSelect, send: true)
            navigationController?.popViewController(animated: true)
        }else{
            AppDelegate.showAlertView(vc: self, titleString: "", messageString: "Por favor seleccione uno o más tipos de servicios que desea reportar")
        }
    }
    
    func OperatorServicesSelected(idTipoServicio: Int, TipoServicio: String, state: Bool) {
        
        if state{
            if TipoServicio != ""{
                TipoServicioSelect.append(TipoServicio)
                idTipoServicioSelect.append(idTipoServicio)
            }
        }else{
            if let index = TipoServicioSelect.index(of:TipoServicio) {
                TipoServicioSelect.remove(at: index)
                idTipoServicioSelect.remove(at: index)
            }
        }
    }
    
    var _servicioOperador = ServicioOperador()
    var idOperator = ""
    var TipoServicioSelect = [String]()
    var idTipoServicioSelect = [Int]()
    weak var delegate : parseServiceDelegate?
    //weak var services
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Seleccionar el tipo de servicio(s)"
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
            UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)

        print("idTipoServicioSelect \(idTipoServicioSelect)")
        print("TipoServicioSelect \(TipoServicioSelect)")
//        attempFetchServicioOperador(operador: idOperator)
        attempFetchServicioOperadorEqual(operador: idOperator)
    }
    
    func attempFetchServicioOperador(operador: String){
        DataService.shared.requestFetchServiciosOperador(with: operador){ [weak self] (servicio, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let servicio = servicio else{
                return
            }
            print("\(servicio)")
            self?.updateServicioOperador(with: servicio)
        }
    }
    
    func attempFetchServicioOperadorEqual(operador: String){
        DataService.shared.requestFetchServiciosOperadorEqual(with: operador){ [weak self] (servicio, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let servicio = servicio else{
                return
            }
            print("\(servicio)")
            self?.updateServicioOperador(with: servicio)
        }
    }
    
    func updateServicioOperador(with servicio: ServicioOperador) {
        _servicioOperador = servicio
        stopAnimating()
        //_provincia.insert(ProvinciaElement.init(codigo: "0", provincia: "Seleccione" ), at: 0)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return _servicioOperador.count + 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == _servicioOperador.count - 1{
            return 80
        }
        
        return 65
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        if indexPath.row == _servicioOperador.count {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNextAction", for: indexPath) as! ServicesSendTableViewCell
            cell.delegate = self
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOperatorServices", for: indexPath) as! OperatorServicesTableViewCell
        cell.delegate = self
        cell.setServicio(servicio: _servicioOperador[indexPath.row], idSelected: idTipoServicioSelect)
        
        return cell
    }
    

}
