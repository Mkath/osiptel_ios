//
//  ReportCoverageViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/27/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

class ReportCoverageViewController: UIViewController, NVActivityIndicatorViewable,parseServiceDelegate {
    
    @IBOutlet weak var viewcontainerContinau: UIView!
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtApellidoM: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtOperadora: UITextField!
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtTipoProblema: UITextField!
    public var PickerOperadora : UIPickerView?
    public var PickerFecha : UIDatePicker?
    @IBOutlet weak var infoButton: UIImageView!
    
    @IBOutlet weak var txtDepartamento: UITextField!
    @IBOutlet weak var txtProvincia: UITextField!
    @IBOutlet weak var txtDistrito: UITextField!
    @IBOutlet weak var txtLocalidad: UITextField!
    @IBOutlet weak var txtviewTipoServicio: UITextView!
    
    @IBOutlet weak var viewTipoServicio: UIView!
    var txtFieldActive = 0
    var idEmpresa: Int = 0
    var idTipoProblema: Int = 0
    var isValidate = false
    
    var latitudActual : Double = 0
    var longitudActual : Double = 0
    
    public var PickertipoProblema : UIPickerView?
    public var PickerDepartamento : UIPickerView?
    public var PickerProvincia : UIPickerView?
    public var PickerDistrito : UIPickerView?
    public var PickerLocalidad : UIPickerView?
    
    var visualEffect:UIVisualEffect!
    var _empresa = Operador()
    var _tipoProblema = TipoProblema()
    var postCobertura : PostCobertura!
    
    var _departamento = Departamento()
    var _provincia = Provincia()
    var _distrito = Distrito()
    var _localidad = Localidad()
    var _ubicacion = Ubicacion()
    
    var idDepartamento = ""
    var idProvincia = ""
    var idDistrito = ""
    
    var _departamentoCadena = "0"
    var _provinciaCadena = "0"
    var _distritoCadena = "0"
    var _localidaCadena = "0"
    
    
    var DepartamentoSegue = ""
    var ProvinciaSegue = ""
    var DistritoSegue = ""
    var localidadSegue = ""
    var LatitudSegue = ""
    var LongitudSegue = ""
    
    
    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = UserDefaults.standard.getNameComplete()
    
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    let myColor = UIColor.red
    var OperatorServicesTableViewController : OperatorServicesTableViewController?
    var idTipoServicioSelected = [Int]()
    var TipoServicioSelect = [String]()
    
    var _latitudConsultada : Float = 0.0
    var _longitudConsultada : Float = 0.0
    
    var SelecttipoServicio = false
    var sendState = false
    func SendAllServiceSelected(tipoServicio: [String], idTipoServicio: [Int], send: Bool) {
        let slected = tipoServicio.joined(separator:",")
        idTipoServicioSelected = idTipoServicio
        sendState = send
        TipoServicioSelect = tipoServicio
        txtviewTipoServicio.text = slected
        viewTipoServicio.alpha = 1
        viewTipoServicio.isHidden = false
        viewTipoServicio.frame.origin.y = 804
        
        PickerFecha = UIDatePicker()
        PickerFecha?.datePickerMode = .dateAndTime
        PickerFecha?.maximumDate = Date()
        PickerFecha?.addTarget(self, action: #selector(dateTimeChanged(datePicker:)), for: .valueChanged)
        txtFecha?.inputView =  PickerFecha
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.dateFormat = "dd/MM/YYYY HH:mm"
        txtFecha.text = formatter.string(from: Date())
        viewcontainerContinau.frame.origin.y = 914
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !sendState{
            if idTipoServicioSelected.count == 0 {
                txtviewTipoServicio.text = ""
                txtTipoProblema.text = "Seleccione"
                txtOperadora.text = "Seleccione"
                viewTipoServicio.alpha = 0
                viewTipoServicio.isHidden = true
                viewcontainerContinau.frame.origin.y = 804
                PickerFecha = UIDatePicker()
                PickerFecha?.datePickerMode = .date
                PickerFecha?.maximumDate = Date()
                PickerFecha?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
                txtFecha?.inputView =  PickerFecha
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                formatter.dateFormat = "dd/MM/YYYY"
                txtFecha.text = formatter.string(from: Date())
            }else{
                viewTipoServicio.alpha = 0
                viewTipoServicio.isHidden = true
                viewcontainerContinau.frame.origin.y = 804
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let size = CGSize(width: 308, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        let item = UIBarButtonItem(title: "Reportar", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        
        self.title = "Reportar"
        self.navigationItem.title = "Reportar"
        
        txtNombres.delegate = self
        txtNombres.tag = 0
        txtApellidos.delegate = self
        txtApellidos.tag = 1
        txtApellidoM.delegate = self
        txtApellidoM.tag = 2
        txtTelefono.delegate = self
        txtTelefono.tag = 3
        
        txtOperadora.delegate = self
        txtOperadora.tag = 4
        
        txtFecha.delegate = self
        
        PickerFecha?.tag = 5
        
        PickerFecha = UIDatePicker()
        PickerFecha?.datePickerMode = .date
        PickerFecha?.maximumDate = Date()
        PickerFecha?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        
        viewcontainerContinau.frame.origin.y = 804
        
        let tapGesturetex = UITapGestureRecognizer(target: self, action: #selector(tapOnTextView(_:)))
        txtviewTipoServicio.addGestureRecognizer(tapGesturetex)

        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        let infoTap = UITapGestureRecognizer(target: self, action: #selector(tapInfo))
        view.addGestureRecognizer(tapGesture)
        
        infoButton.isUserInteractionEnabled = true
        infoButton.addGestureRecognizer(infoTap)
        
        print("_latitudConsultada ")
        print(_latitudConsultada)
        
        print("_longitudConsultada ")
        print(_longitudConsultada)
        
        txtEmail.delegate = self
        txtEmail.tag = 6
        
        txtTipoProblema.delegate = self
        txtTipoProblema.tag = 7
        txtFecha?.inputView =  PickerFecha
        //Ubigeo
        txtDepartamento.delegate = self
        txtDepartamento.text = "Seleccione"
        txtDepartamento.tag = 8
        txtProvincia.delegate = self
        txtProvincia.text = "Seleccione"
        txtProvincia.tag = 9
        txtDistrito.delegate = self
        txtDistrito.text = "Seleccione"
        txtDistrito.tag = 10
        txtLocalidad.delegate = self
        txtLocalidad.text = "Seleccione"
        txtLocalidad.tag = 11
        
        txtDepartamento.isEnabled = true
        txtProvincia.isEnabled = false
        txtDistrito.isEnabled = false
        txtLocalidad.isEnabled = false
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = manager.location else {
                return
            }
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
            latitudActual = currentLocation.coordinate.latitude
            longitudActual = currentLocation.coordinate.longitude
        }
        
        attempFetchTipoProblema()
        attempFetchDepartamento()
        
        
        txtNombres.text = UserDefaults.standard.getName()
        txtApellidos.text = UserDefaults.standard.getLastnameP()
        txtApellidoM.text = UserDefaults.standard.getLastnameM()
        txtEmail.text = UserDefaults.standard.getEmail()
        txtTelefono.text = UserDefaults.standard.getNumber()
        
        if DepartamentoSegue.isEmpty{
            txtDepartamento.text = "Seleccione"
            txtProvincia.text = "Seleccione"
            txtDistrito.text = "Seleccione"
            txtLocalidad.text = "Seleccione"
        }else{
            txtDepartamento.text = DepartamentoSegue
        }
        txtProvincia.text = ProvinciaSegue
        txtDistrito.text = DistritoSegue
        txtLocalidad.text = localidadSegue
        OperatorServicesTableViewController?.delegate = self
        
        txtOperadora.text = "Seleccione"
        txtTipoProblema.text = "Seleccione"
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.dateFormat = "dd/MM/yyyy"
        txtFecha.text = formatter.string(from: Date())
        
        txtviewTipoServicio.text = ""
        txtviewTipoServicio.tag = 200
        txtTipoProblema.text = "Seleccione"
        txtOperadora.text = "Seleccione"
        
        if DepartamentoSegue != ""{
            let testUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(BackButton))
            self.navigationItem.leftBarButtonItem  = testUIBarButtonItem
        }        
    }
    
    @objc func BackButton(){
        print("button click")
        self.dismiss(animated: true, completion: nil)
    }

    
    @objc private final func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        if txtTipoProblema.text == "Interrupción"{
            if txtOperadora.text != ""{
                let sb = UIStoryboard(name: "Main", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "OperatorServicesTableViewController") as! OperatorServicesTableViewController
                let backItem = UIBarButtonItem()
                vc.delegate = self
                backItem.title = " "
                vc.idOperator = String(idEmpresa)
                print("idOperator \(vc.idOperator)")
                vc.idTipoServicioSelect = idTipoServicioSelected
                vc.TipoServicioSelect = TipoServicioSelect
                print("idOperator \(vc.idTipoServicioSelect)")
                navigationItem.backBarButtonItem = backItem
                    self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    
    func attempFetchDepartamento(){
        DataService.shared.requestFetchAllDepartamento{ [weak self] (departamento, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let departamento = departamento else{
                return
            }
            self?.updateDepartamento(with: departamento)
        }
    }
    
    func updateDepartamento(with departamento: Departamento) {
        _departamento = departamento
        _departamento.insert(DepartamentoElement.init(codigo: "0", departamento: "Seleccione"), at: 0)
        stopAnimating()
    }
    
    //Provincia
    func attempFetchProvincia(idDepartmento: String){
        DataService.shared.requestFetchProvincia(with: idDepartmento){ [weak self] (provincia, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let provincia = provincia else{
                return
            }
            print("\(provincia)")
            self?.updateProvincia(with: provincia)
        }
    }
    
    func updateProvincia(with provincia: Provincia) {
        _provincia = provincia
        _provincia.insert(ProvinciaElement.init(codigo: "0", provincia: "Seleccione" ), at: 0)
        PickerProvincia?.reloadAllComponents()
    }
    
    //Distrito
    func attempFetchDistrito(idDepartmento: String, idProvincia: String){
        DataService.shared.requestFetchDistrito(with: idDepartmento, with: idProvincia){ [weak self] (distrito, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let distrito = distrito else{
                return
            }
            
            print("\(distrito)")
            self?.updateDistrito(with: distrito)
        }
    }
    
    func updateDistrito(with distrito: Distrito) {
        _distrito = distrito
        //_provincia.insert(ProvinciaElement.init(codigo: "00",departamento: "", provincia: "Todos" ), at: 0)
        _distrito.insert(DistritoElement.init(codigo: "0",distrito: "Seleccione"), at: 0)
        PickerDistrito?.reloadAllComponents()
     
    }
    
    //Localidad
    func attempFetchLocalidad(idDepartmento: String, idProvincia: String, idDistrito: String){
        DataService.shared.requestFetchLocalidad(with: idDepartmento, with: idProvincia, with: idDistrito){ [weak self] (localidad, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let localidad = localidad else{
                return
            }
            self?.updateLocalidad(with: localidad)
        }
    }
    
    func updateLocalidad(with localidad: Localidad) {
        _localidad = localidad
        _localidad.insert(LocalidadElement.init(codigo: "0", localidad: "Seleccione"), at: 0)
        PickerLocalidad?.reloadAllComponents()
    }
    
    @objc func tapInfo(sender:UITapGestureRecognizer) {
        alertInformationViewController()
    }
    
    func attempFetchEmpresa(){
        DataService.shared.requestFetchOperadores{ [weak self] (empresa, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let empresa = empresa else{
                return
            }
            self?.updateEmpresa(with: empresa)
        }
    }
    
    func updateEmpresa(with empresa: Operador) {
        _empresa = empresa
        _empresa.insert(OperadorElement.init(idOperador: 0, nombreComercial: "Seleccione", razonSocial: "", enlace: "", nombreImagen: "", codigoOperador: ""), at: 0)
        PickerOperadora?.reloadAllComponents()
        
    }
    
    
    func attempFetchTipoProblema(){
        DataService.shared.requestFetchTipoProblema{ [weak self] (tipoProblema, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let tipoProblema = tipoProblema else{
                return
            }
            self?.updateTipoProblema(with: tipoProblema)
        }
    }
    
    func updateTipoProblema(with tipoProblema: TipoProblema) {
        _tipoProblema = tipoProblema
        _tipoProblema.insert(TipoProblemaElement.init(idTipoProblema: 0, tipoProblema: "Seleccionar"), at: 0)
        PickertipoProblema?.reloadAllComponents()
    }

    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }

    @objc func dateChanged(datePicker: UIDatePicker){
        txtFecha.layer.borderWidth = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        txtFecha.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    
    @objc func dateTimeChanged(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY HH:mm"
        txtFecha.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        
        isValidate = true
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.dateFormat = "dd/MM/YYYY"
        let fechaActual = formatter.string(from: Date())
        _ = formatter.date(from: txtFecha.text!)
        _ = formatter.date(from: fechaActual)

        if txtNombres.text?.count == 0{
            isValidate = false
            txtNombres.borderStyle = UITextBorderStyle.roundedRect
            txtNombres.layer.borderColor = myColor.cgColor
            txtNombres.layer.cornerRadius = 4
            txtNombres.borderColorView = UIColor.red
            txtNombres.layer.borderWidth = 1.0
        }
        
        if txtApellidos.text?.count == 0{
            isValidate = false
            txtApellidos.borderStyle = UITextBorderStyle.roundedRect
            txtApellidos.layer.borderColor = myColor.cgColor
            txtApellidos.layer.cornerRadius = 4
            txtApellidos.borderColorView = UIColor.red
            txtApellidos.layer.borderWidth = 1.0
        }
        
        if txtApellidoM.text?.count == 0{
            isValidate = false
            txtApellidoM.borderStyle = UITextBorderStyle.roundedRect
            txtApellidoM.layer.borderColor = myColor.cgColor
            txtApellidoM.layer.cornerRadius = 4
            txtApellidoM.borderColorView = UIColor.red
            txtApellidoM.layer.borderWidth = 1.0
        }
        
        if txtTelefono.text!.count < 8 {
            isValidate = false
            txtTelefono.borderStyle = UITextBorderStyle.roundedRect
            txtTelefono.layer.borderColor = myColor.cgColor
            txtTelefono.layer.cornerRadius = 4
            txtTelefono.borderColorView = UIColor.red
            txtTelefono.layer.borderWidth = 1.0
        }
        
        if txtFecha.text?.count == 0{
            isValidate = false
            txtFecha.borderStyle = UITextBorderStyle.roundedRect
            txtFecha.layer.borderColor = myColor.cgColor
            txtFecha.layer.cornerRadius = 4
            txtFecha.borderColorView = UIColor.red
            txtFecha.layer.borderWidth = 1.0
        }
        
        if txtEmail.text!.isEmpty{
            isValidate = false
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = myColor.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
        }
        
        if !isValidEmailAddress(emailAddressString: txtEmail.text!){
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            isValidate = false
        }
        
        
        if txtOperadora.text?.count == 0 || txtOperadora.text == "Seleccione"{
            isValidate = false
            txtOperadora.borderStyle = UITextBorderStyle.roundedRect
            txtOperadora.layer.borderColor = myColor.cgColor
            txtOperadora.layer.cornerRadius = 4
            txtOperadora.borderColorView = UIColor.red
            txtOperadora.layer.borderWidth = 1.0
        }

        if txtTipoProblema.text?.count == 0 || txtTipoProblema.text == "Seleccione"{
            isValidate = false
            txtTipoProblema.borderStyle = UITextBorderStyle.roundedRect
            txtTipoProblema.layer.borderColor = myColor.cgColor
            txtTipoProblema.layer.cornerRadius = 4
            txtTipoProblema.borderColorView = UIColor.red
            txtTipoProblema.layer.borderWidth = 1.0
        }
        
        if txtDepartamento.text?.count == 0 || txtDepartamento.text == "Seleccione"{
            isValidate = false
            txtDepartamento.borderStyle = UITextBorderStyle.roundedRect
            txtDepartamento.layer.borderColor = myColor.cgColor
            txtDepartamento.layer.cornerRadius = 4
            txtDepartamento.borderColorView = UIColor.red
            txtDepartamento.layer.borderWidth = 1.0

        }
        
        if txtProvincia.text?.count == 0 || txtProvincia.text == "Seleccione"{
            isValidate = false
            txtProvincia.borderStyle = UITextBorderStyle.roundedRect
            txtProvincia.layer.borderColor = myColor.cgColor
            txtProvincia.layer.cornerRadius = 4
            txtProvincia.borderColorView = UIColor.red
            txtProvincia.layer.borderWidth = 1.0
        }
        
        if txtDistrito.text?.count == 0 || txtDistrito.text == "Seleccione"{
            isValidate = false
            txtDistrito.borderStyle = UITextBorderStyle.roundedRect
            txtDistrito.layer.borderColor = myColor.cgColor
            txtDistrito.layer.cornerRadius = 4
            txtDistrito.borderColorView = UIColor.red
            txtDistrito.layer.borderWidth = 1.0
        }
        
        if txtLocalidad.text?.count == 0 || txtLocalidad.text == "Seleccione"{
            isValidate = false
            txtLocalidad.borderStyle = UITextBorderStyle.roundedRect
            txtLocalidad.layer.borderColor = myColor.cgColor
            txtLocalidad.layer.cornerRadius = 4
            txtLocalidad.borderColorView = UIColor.red
            txtLocalidad.layer.borderWidth = 1.0
        }

        if isValidate{
            let size = CGSize(width: 308, height: 30)
            startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                , fadeInAnimation: nil)
            
            if _latitudConsultada == 0 && _longitudConsultada == 0{
                attempFetchUbicacion(idDepartmento: txtDepartamento.text!, idProvincia: txtProvincia.text!, idDistrito: txtDistrito.text!, localidad: txtLocalidad.text!)
            }else{
                reportMapVC()
            }
            //}
        }
    }
    
    
    func reportMapVC(){
        stopAnimating()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportMapViewController") as! ReportMapViewController
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        
        let idTipoServicio = idTipoServicioSelected.map { String($0) }
        
        controller.nombres = txtNombres.text!
        controller.apellidos = txtApellidos.text!
        controller.apellidosM = txtApellidoM.text!
        controller.telefono = txtTelefono.text!
        controller.idEmpresa = idEmpresa
        controller.fecha = txtFecha.text!
        controller.email = txtEmail.text!
        controller.idServicio = idTipoServicio.joined(separator: ",")
        controller.idTipoProblema = idTipoProblema
        controller._departamentoCadena = txtDepartamento.text!
        controller._provinciaCadena = txtProvincia.text!
        controller._distritoCadena = txtDistrito.text!
        controller._localidaCadena = txtLocalidad.text!
        
        controller._latitudConsultada = _latitudConsultada
        controller._longitudConsultada = _longitudConsultada
        
        _latitudConsultada = 0
        _longitudConsultada = 0
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func attempFetchUbicacion(idDepartmento: String, idProvincia: String, idDistrito: String, localidad: String){
        
        DataService.shared.requestFetchGetUbicacionActual(with: idDepartmento, with: idProvincia, with: idDistrito, with: localidad){ [weak self] (ubicacion, error) in
            if let error = error {
                self!.stopAnimating()
            }
            guard let ubicacion = ubicacion else{
                return
            }
            self?.updateUbicacion(with: ubicacion)
        }
    }
    
    func updateUbicacion(with ubicacion: Ubicacion) {
        
        
        _ubicacion = ubicacion
        
        if _ubicacion.count > 0{
            let lat = _ubicacion[0].latitud
            let long = _ubicacion[0].longitud
            
            _latitudConsultada = Float(lat) ?? 0
            _longitudConsultada = Float(long) ?? 0
            
            reportMapVC()
        }
    }
    
    func attempFetchPostCobertura(withId post: PostCobertura){
        let size = CGSize(width: 308, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        DataService.shared.requestFetchCreateReporteCobertura(with: post) {[weak self] (response, error,String, Mensaje ) in
            self?.stopAnimating()
            
            if let error = error {
                self!.alertViewController(titulo: "", mensaje: "Intente nuevamente", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                return
            }
            
            if String == "200"{
                self!.alertViewController(titulo: "", mensaje: "Su reporte \(Mensaje!) se ha generado con éxito", closeFlag: true, imageValidate: true, closeAutomatic: false, sendVC: "Señal")
            }else if String == "500"{
                self!.alertViewController(titulo: "", mensaje: "\(Mensaje!)", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }else if String == "400"{
                self!.alertViewController(titulo: "", mensaje: "\(Mensaje!)", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }
        }
    }
}

extension ReportCoverageViewController: UITextFieldDelegate,UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.tag == 200{
            txtviewTipoServicio.layer.borderWidth = 0.0
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 0{
            txtNombres.layer.borderWidth = 0
        }
        
        if textField.tag == 1{
            txtApellidos.layer.borderWidth = 0
        }
        
        if textField.tag == 2{
            txtApellidoM.layer.borderWidth = 0
        }
        
        if textField.tag == 3{
            txtTelefono.layer.borderWidth = 0
        }
        
        if textField.tag == 5{
            txtFecha.layer.borderWidth = 0
        }
        
        if textField.tag == 6{
            txtEmail.layer.borderWidth = 0
        }
        
        if textField.tag == 7{
            txtTipoProblema.layer.borderWidth = 0
        }

        
        if textField.tag == 3{
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }

        if textField.tag == 0 || textField.tag == 1 || textField.tag == 2{
            
            let allowedCharacters = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func pickup(_ textField : UITextField){

        print("Textfield \(textField.tag)")
        
        if textField.tag == 4{
            
            PickerOperadora = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickerOperadora = UIPickerView()
            PickerOperadora?.delegate = self
            PickerOperadora?.dataSource = self
            PickerOperadora?.backgroundColor = UIColor.white
            txtOperadora.inputView = PickerOperadora
        }

        if textField.tag == 7{
            
            PickertipoProblema = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickertipoProblema = UIPickerView()
            PickertipoProblema?.delegate = self
            PickertipoProblema?.dataSource = self
            PickertipoProblema?.backgroundColor = UIColor.white
            txtTipoProblema.inputView = PickertipoProblema
        }
        
        if textField.tag == 8{
            
            PickerDepartamento = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickerDepartamento = UIPickerView()
            PickerDepartamento?.delegate = self
            PickerDepartamento?.dataSource = self
            PickerDepartamento?.backgroundColor = UIColor.white
            txtDepartamento.inputView = PickerDepartamento
        }
        
        if textField.tag == 9{
            
            PickerProvincia = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickerProvincia = UIPickerView()
            PickerProvincia?.delegate = self
            PickerProvincia?.dataSource = self
            PickerProvincia?.backgroundColor = UIColor.white
            txtProvincia.inputView = PickerProvincia

        }
        
        if textField.tag == 10{
            
            PickerDistrito = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickerDistrito = UIPickerView()
            PickerDistrito?.delegate = self
            PickerDistrito?.dataSource = self
            PickerDistrito?.backgroundColor = UIColor.white
            txtDistrito.inputView = PickerDistrito
        }
        
        if textField.tag == 11{
            
            PickerLocalidad = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            PickerLocalidad = UIPickerView()
            PickerLocalidad?.delegate = self
            PickerLocalidad?.dataSource = self
            PickerLocalidad?.backgroundColor = UIColor.white
            txtLocalidad.inputView = PickerLocalidad
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
        toolBar.sizeToFit()
        
        //Adding
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    

    @objc func doneClick(_ sender : UITextField){
        
        print("Textfield \(txtFieldActive)")
        if txtFieldActive == 4{
            txtOperadora.resignFirstResponder()
        }
        
        if txtFieldActive == 7{
            txtTipoProblema.resignFirstResponder()
        }
        
        if txtFieldActive == 8{
            txtDepartamento.resignFirstResponder()
        }
        
        if txtFieldActive == 9{
            txtProvincia.resignFirstResponder()
        }
        
        if txtFieldActive == 10{
            txtDistrito.resignFirstResponder()
        }
        
        if txtFieldActive == 11{
            txtLocalidad.resignFirstResponder()
        }
    }
    
    @objc func cancelClick(_ sender : UITextField){
        
        print("Textfield \(sender.tag)")
        if txtFieldActive == 4{
            txtOperadora.resignFirstResponder()
        }
        
        if txtFieldActive == 7{
            txtTipoProblema.resignFirstResponder()
        }
        
        if txtFieldActive == 8{
            txtDepartamento.resignFirstResponder()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        txtFieldActive = 0
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
        if textField.tag == 4{
            txtFieldActive = 4
            txtOperadora.layer.borderWidth = 0
            self.pickup(txtOperadora)
        }
        
        if textField.tag == 5{
            txtFecha.layer.borderWidth = 0            
        }
        
        if textField.tag == 7{
            txtFieldActive = 7
            txtTipoProblema.layer.borderWidth = 0
            self.pickup(txtTipoProblema)
        }
        if textField.tag == 8{
            txtFieldActive = 8
            txtDepartamento.layer.borderWidth = 0
            self.pickup(txtDepartamento)
        }
        if textField.tag == 9{
            txtFieldActive = 9
            txtProvincia.layer.borderWidth = 0
            self.pickup(txtProvincia)
        }
        if textField.tag == 10{
            txtFieldActive = 10
            txtDistrito.layer.borderWidth = 0
            self.pickup(txtDistrito)
        }
        if textField.tag == 11{
            txtFieldActive = 11
            txtLocalidad.layer.borderWidth = 0
            self.pickup(txtLocalidad)
        }
        if textField.tag == 5{
            txtFieldActive = 5
            txtFecha.layer.borderWidth = 0
        }

    }
}

extension ReportCoverageViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if txtFieldActive == 4{
            txtFieldActive = 4
            self.pickup(txtOperadora)
            return _empresa.count
        }
        if txtFieldActive == 7{
            txtFieldActive = 7
            self.pickup(txtTipoProblema)
            return _tipoProblema.count
        }
        if txtFieldActive == 8{
            txtFieldActive = 8
            self.pickup(txtDepartamento)
            return _departamento.count
        }
        if txtFieldActive == 9{
            txtFieldActive = 9
            if _provincia.count > 0{
                txtProvincia.isEnabled = true
                self.pickup(txtProvincia)
            }
            return _provincia.count
        }
        if txtFieldActive == 10{
            txtFieldActive = 10
            if _distrito.count > 0{
                txtDistrito.isEnabled = true
                self.pickup(txtDistrito)
            }
            return _distrito.count
        }
        if txtFieldActive == 11{
            txtFieldActive = 11
            if _localidad.count > 0{
                txtLocalidad.isEnabled = true
                self.pickup(txtLocalidad)
            }
            return _localidad.count
        }
        

        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if txtFieldActive  == 4{
            return _empresa[row].nombreComercial
        }
        if txtFieldActive == 7{
            return _tipoProblema[row].tipoProblema
        }
        if txtFieldActive == 8{
            return _departamento[row].departamento
        }
        if txtFieldActive == 9{
            return _provincia[row].provincia
        }
        if txtFieldActive == 10{
            return _distrito[row].distrito
        }
        if txtFieldActive == 11{
            return _localidad[row].localidad
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        if txtFieldActive == 4{
            if row != 0{
                if  txtOperadora.text != "" ||  txtOperadora.text != "Seleccione" {
                    txtOperadora.text = _empresa[row].nombreComercial
                    idEmpresa = Int(_empresa[row].codigoOperador)!
                
                    if txtTipoProblema.text == "Interrupción"{
                        self.view.endEditing(true)
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "OperatorServicesTableViewController") as! OperatorServicesTableViewController
                        let backItem = UIBarButtonItem()
                        vc.delegate = self
                        backItem.title = " "
                        vc.idOperator = String(idEmpresa)
                        navigationItem.backBarButtonItem = backItem
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        PickerFecha = UIDatePicker()
                        PickerFecha?.datePickerMode = .date
                        PickerFecha?.maximumDate = Date()
                        PickerFecha?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
                        txtFecha?.inputView =  PickerFecha
                        let formatter = DateFormatter()
                        formatter.dateStyle = .medium
                        formatter.dateFormat = "dd/MM/yyyy"
                        txtFecha.text = formatter.string(from: Date())
                    }
                }
            }
        }
        if txtFieldActive == 7{
            txtTipoProblema.text = _tipoProblema[row].tipoProblema
            idTipoProblema = _tipoProblema[row].idTipoProblema
            txtOperadora.text = ""
            attempFetchEmpresa()
            
            if txtTipoProblema.text == "Cobertura"{
                viewTipoServicio.alpha = 0
                viewTipoServicio.isHidden = true
                viewcontainerContinau.frame.origin.y = 804
                
                PickerFecha = UIDatePicker()
                PickerFecha?.datePickerMode = .date
                PickerFecha?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
                txtFecha?.inputView =  PickerFecha
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                formatter.dateFormat = "dd/MM/yyyy"
                txtFecha.text = formatter.string(from: Date())
            }
        }
        if txtFieldActive == 8{
            txtDepartamento.text = _departamento[row].departamento
             idDepartamento = _departamento[row].departamento
            attempFetchProvincia(idDepartmento: idDepartamento)
            txtDistrito.text = ""
            txtLocalidad.text = ""
            txtProvincia.text = "Seleccione"
            txtProvincia.isEnabled = true
            txtDistrito.isEnabled = false
            txtLocalidad.isEnabled = false
            
            PickerProvincia?.reloadAllComponents()
            PickerDistrito?.reloadAllComponents()
            PickerLocalidad?.reloadAllComponents()
        }
        if txtFieldActive == 9{
            txtProvincia.text = _provincia[row].provincia
            idProvincia = _provincia[row].provincia
            attempFetchDistrito(idDepartmento: idDepartamento, idProvincia: idProvincia)
            txtLocalidad.text = ""
            txtDistrito.text = "Seleccione"
            txtDistrito.isEnabled = true
            PickerDistrito?.reloadAllComponents()
            PickerLocalidad?.reloadAllComponents()
        }
        if txtFieldActive == 10{
            txtDistrito.text = _distrito[row].distrito
            idDistrito = _distrito[row].distrito
            attempFetchLocalidad(idDepartmento: idDepartamento, idProvincia: idProvincia, idDistrito: idDistrito)
            txtLocalidad.text = "Seleccione"
            txtLocalidad.isEnabled = true
            PickerLocalidad?.reloadAllComponents()
        }
        if txtFieldActive == 11{
            if _localidad.count > 0 {
                txtLocalidad.text = _localidad[row].localidad
            }
        }
    }
}
