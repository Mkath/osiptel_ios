//
//  ReportMapViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/27/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import NVActivityIndicatorView
import GoogleMaps

private let kPersonWishListAnnotationName = "kPersonWishListAnnotationName"

class ReportMapViewController: UIViewController,NVActivityIndicatorViewable{


    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    var locality = ""
    var administrativeArea = ""
    var country = ""
    var previousLocation: CLLocation!
    
    
    //Parse Report
    var nombres : String = ""
    var apellidos : String = ""
    var apellidosM : String = ""
    var telefono : String = ""
    var idEmpresa : Int = 0
    var fecha : String = ""
    var email : String = ""
    var idTipoProblema : Int = 0
    
    var latitudeMap : Float = 0
    var longitudeMap : Float = 0
    
    var latitudeMapUsuario : Double = 0
    var longitudeMapUsuario : Double = 0
    
    var _departamentoCadena = ""
    var _provinciaCadena = ""
    var _distritoCadena = ""
    var _localidaCadena = ""    
    var idServicio = ""
    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = UserDefaults.standard.getNameComplete()
    
    var latitudActual : Float = 0
    var longitudActual : Float = 0
    var _latitudConsultada : Float = 0
    var _longitudConsultada : Float = 0
    
    @IBOutlet weak var viewLocationMap: UIView!
    @IBOutlet weak var lblUbicacion: UILabel!
    @IBOutlet weak var ReportMap: MKMapView!
    
    @IBAction func btnReportAction(_ sender: UIButton) {
        
        let _post = PostCobertura.init(nombre: nombres, apellidoPaterno: apellidos, apellidoMaterno: apellidosM, numeroTelefono: telefono, marcaEquipo: "Apple", modeloEquipo: UserDefaults.standard.getModelDevice(), sistemaOperativo: "iOS", idEmpresa: idEmpresa, fechaReporte: fecha, correoElectronico: email, idProblema: idTipoProblema, departamento: _departamentoCadena, provincia: _provinciaCadena, distrito: _distritoCadena, localidad: _localidaCadena, latitudReporte: String(latitudeMap), longitudReporte: String(longitudeMap), latitudUbicacionPersona: String(latitudActual), longitudUbicacionPersona: String(longitudActual),idServicio: idServicio, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, mac: MAC, nombreUsuario: nombreUsuario)
        
        attempFetchPostCobertura(withId: _post)
    }
    
    func attempFetchPostCobertura(withId post: PostCobertura){
        let size = CGSize(width: 308, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        DataService.shared.requestFetchCreateReporteCobertura(with: post) {[weak self] (response, error, string, Mensaje)  in

            self?.stopAnimating()
            if error != nil {
            
                self!.alertViewController(titulo: "", mensaje: "Intente nuevamente", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                return
            }
            
            if string == "200"{
                self!.alertViewController(titulo: "", mensaje: "Su reporte \(Mensaje!) se ha generado con éxito", closeFlag: true, imageValidate: true, closeAutomatic: false, sendVC: "Señal")
            }else if string == "500"{
                self!.alertViewController(titulo: "", mensaje: "\(Mensaje!)", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }else if string == "400"{
                self!.alertViewController(titulo: "", mensaje: "\(Mensaje!)", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let mapLatitude = ReportMap.centerCoordinate.latitude
        let mapLongitude = ReportMap.centerCoordinate.longitude
        
        let span = MKCoordinateSpanMake(0.075, 0.075)
        var  latitudInicial = Float(mapLatitude)
        var longitudInicial = Float(mapLongitude)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnLocation(_:)))
        viewLocationMap.addGestureRecognizer(tapGesture)
        
        if _longitudConsultada != 0 && _latitudConsultada != 0  {
            latitudInicial = _latitudConsultada
            longitudInicial = _longitudConsultada
        }
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitudInicial), longitude: CLLocationDegrees(longitudInicial)), span: span)
            self.ReportMap.setRegion(region, animated: true)
            manager.requestWhenInUseAuthorization()
  
            if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                guard let currentLocation = manager.location else {
                    return
                }

                latitudActual = Float(currentLocation.coordinate.latitude)
                longitudActual = Float(currentLocation.coordinate.longitude)
                if _longitudConsultada != 0 && _latitudConsultada != 0  {
                    latitudActual = _latitudConsultada
                    longitudActual = _longitudConsultada
                }
            }
            
            let geoCoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(latitudActual), CLLocationDegrees(longitudActual))
            
        _ = String()
            
            geoCoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    _ = address.administrativeArea
                    _ = address.country
                    _ = address.subLocality
                    _ = address.locality
                    _ = address.thoroughfare
                    _ = address.postalCode
                    let lines = address.lines! as [String]
                    
                    let address = lines.first as! String
                    //address = address.components (separatedBy:  ",")    [0]
                    self.lblUbicacion.text = "\(address)"
                }
            }
        checkLocationServices()
    }
    
    
    @objc private final func tapOnLocation(_ tapGesture: UITapGestureRecognizer){
        centerViewOnUserLocation()
    }

    func setupLocationManager(){
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }

    func centerViewOnUserLocation(){
        
        if let location = manager.location?.coordinate{
            let span = MKCoordinateSpan.init(latitudeDelta: 0.0075, longitudeDelta: 0.0075)
            let region = MKCoordinateRegion.init(center: location, span: span)
            ReportMap.setRegion(region, animated: true)
        }
    }
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAutorization()
        }else{
            //Mostrar alerta al usuario
        }
    }
    
    
    func getCenterPosition(for mapView: MKMapView) -> CLLocation{
        
        let center = mapView.centerCoordinate
        let mapLatitude = center.latitude
        let mapLongitude = center.longitude
        
        return CLLocation(latitude: mapLatitude, longitude: mapLongitude)
    }
    

    func userLocationString() -> String {
        let userLocationString = "\(locality), \(administrativeArea), \(country)"
        
        return userLocationString
    }
    
    func checkLocationAutorization(){
        switch  CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTackingUserLocation()
        case .denied:
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .authorizedAlways:
            break
        }
    }
    
    func startTackingUserLocation(){
        ReportMap.showsUserLocation = true
        if _longitudConsultada == 0 && _latitudConsultada == 0  {
            centerViewOnUserLocation()
        }
        manager.startUpdatingLocation()
        previousLocation = getCenterPosition(for: ReportMap)
    }
}


extension ReportMapViewController: MKMapViewDelegate,GMSMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {

        let center = getCenterPosition(for: mapView)
        guard let previousLocation = self.previousLocation else { return }
        
        guard center.distance(from: previousLocation) > 50 else { return }
        self.previousLocation = center
        
        let geoCoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(mapView.centerCoordinate.latitude, mapView.centerCoordinate.longitude)
        
        _ = String()
        
        geoCoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]

                DispatchQueue.main.async {
                    let address = lines.first as! String
                    self.lblUbicacion.text = "\(address)"

                }
            }
        }
    }
}

extension ReportMapViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        print("Location \(location)")
        
        manager.stopUpdatingLocation()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                _ = placemarks![0]
            }
        })
        
        
        let locationLast: CLLocation = locations.last!

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //check
        checkLocationAutorization()
    }
}
