//
//  MyFileTableViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion
import Alamofire
import NVActivityIndicatorView

final class MyFileTableViewController: AccordionTableViewController, searchingDelegate, downloadCell, NVActivityIndicatorViewable {
    func searchAction(estado: String, desde: String, hasta: String) {
        file.removeAll()
        
        startAnimating(size, message: "Obteniendo expediente...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        tableView.reloadData()
        attempFetchExpediente(withId: _login.nroDoc, withId: estado, withId: desde, withId: hasta)
    }
    
    func downloadAction(idExpediente: String, recurso: String) {
        //Descargando Archivo
        startAnimating(size, message: "Obteniendo expediente...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        let idExpedienteGenerado = "\(idExpediente)-\(recurso)"
        attempFetchDescargaExpediente(withId: _login.nroDoc, withId: idExpedienteGenerado, withId: _login.idUsuario, withId: _login.nombre)
    }
    
    //private let file = [String]()
    var file : Array<String> = []
    var _expediente = Expediente()
    var _expedienteItem : ExpedienteElement!
    var _login : ResponseLogin!
    var _tiempoTrasu : TiempoTrasu!
    //var _descarga = ExpedienteDescarga()
    var _descarga : ExpedienteDescarga!
    var titleFile : String = "expediente"
    //var search :SearchViewController = SearchViewController()
    var searchViewController : SearchViewController?
    private let image = UIImage(named: "isotipo")!.withRenderingMode(.alwaysTemplate)
    private let topMessage = ""
    private let bottomMessage = "No se han encontrado reportes"
    let size = CGSize(width: 308, height: 30)

    func attempFetchTrasu(){
        DataService.shared.requestFetchTiempoTrasu(){ [weak self] (tiempo, string) in
            if let error = string {
            print("\(error)")
            }
            guard let tiempo = tiempo else{
                return
            }
            print("\(tiempo)")
            self?.updateUITrasu(with: tiempo)
        }
    }
    
    func updateUITrasu(with tiempo: TiempoTrasu) {
        
        let tiempoTasu = tiempo.first?.tiempo as! Int
        UserDefaults.standard.setTrasuTiempo(value: tiempoTasu)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startAnimating(size, message: "Obteniendo expediente...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        searchViewController?.delegate = self
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(timeout), name: Notification.Name.session, object: nil)
        
        navigationController?.navigationBar.tintColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_exit"), style: .plain, target: self, action: #selector(logOut(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(addTapped(_:)))
        
        
        attempFetchExpediente(withId: _login.nroDoc, withId: "", withId: "", withId: "")
        
        registerCell()
        expandFirstCell()
        
        
    }
    
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = .white        
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }

    @objc func addTapped(_ sender: UIBarButtonItem){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        vc.modalPresentationStyle = .overFullScreen
        vc._login = _login
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }

    @objc func logOut(_ sender: UIBarButtonItem){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LogOutViewController") as! LogOutViewController
            vc.modalPresentationStyle = .overFullScreen
        
            self.present(vc, animated: false, completion: nil)
    }
    
    @objc func timeout(){
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }
    
    func attempFetchExpediente(withId numeroDocumento: String, withId estado: String, withId fechaInicio: String, withId fechaFin: String){
        DataService.shared.requestFetchConsultarExpediente(with: numeroDocumento, with: estado, with: fechaInicio, with: fechaFin){ [weak self] (expediente, error , String) in
         
            self!.stopAnimating()
            if let expediente = expediente{
                print("expediente \(expediente)")
            }
            
            if String == "ok"{
                guard let expediente = expediente else{
                    return
                }
                self?.updateExpediente(with: expediente)
            }else{
                //AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: String)
                
                self!.alertViewController(titulo: "", mensaje: String, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }

            if let error = error {
                print("error \(error.localizedDescription)")

                self!.alertViewController(titulo: global.Titulo, mensaje: global.Mensaje, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                
            }

        }
    }
    
    func updateExpediente(with expediente: Expediente) {
        
        _expediente = expediente
        for modelo in _expediente{
            //sectionNames.append(modelo.idExpediente)
            file.append(modelo.idExpediente)
        }
        
        for datosModelo in _expediente{
            //sectionItems.append([datosModelo])
        }
        tableView.reloadData()
    }

    ///Servicio Descarga
    
    func attempFetchDescargaExpediente(withId numeroDocumento: String, withId idExpediente: String, withId idUsuario: String, withId nombreCompleto: String){
        DataService.shared.requestFetchDescargarExpediente(with: idExpediente, with: numeroDocumento, with: idUsuario, with: nombreCompleto){ [weak self] (url, error,String, Mensaje) in
            self!.stopAnimating()
            if String == "400" {
                //print("error \(error.localizedDescription)")
                self!.alertViewController(titulo: "", mensaje: Mensaje!, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                
                return
            }else if String == "200"{
                self?.updateDescargaExpediente(with: url!)
            }
           /* guard let expediente = expediente else{
                return
            }*/
        }
    }
    
    func updateDescargaExpediente(with url: String) {
        
        //_descarga = expediente
        stopAnimating()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PdfWebViewViewController") as! PdfWebViewViewController
        
        controller.urlExternal = url
        controller.titleFile = "Expediente"
        
        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    // MARK: - Table view data source
    func registerCell() {
        let cellNib = UINib(nibName: MyFileTableViewCell.reuseIdentifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: MyFileTableViewCell.reuseIdentifier)
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 0, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        if file.count > 0{
            tableView.backgroundView = nil
            return file.count
        } else {
            
            
            let backgroundImage = UIImage(named: "osiptel_no")
            let imageView = UIImageView(image: backgroundImage)
            imageView.contentMode = .scaleAspectFit
            imageView.layer.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
            tableView.backgroundView = imageView;
            
        }
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: MyFileTableViewCell.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? MyFileTableViewCell {
            cell.day.text = "EXP. Nº \(file[indexPath.row])"
            cell.setFile(expediente: _expediente[indexPath.row])
            cell.delegate = self
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 275.0 : 65.0
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
