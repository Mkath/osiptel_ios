//
//  MyFileViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/7/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion

class MyFileViewController: UIViewController{


    /*//@IBOutlet weak var TableFiles: UITableView!
    
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    
    var sectionItems2: Array<Any> = []
    var sectionNames2: Array<Any> = []
    
    var _expediente = Expediente()
    var _expedienteItem : ExpedienteElement!
    
    var _login : ResponseLogin!

    override func viewDidLoad() {
        super.viewDidLoad()
        

        //let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(timeout), name: Notification.Name.session, object: nil)

        navigationController?.navigationBar.tintColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: Selector("addTapped"))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filtrar", style: .plain, target: self, action: Selector("addTapped"))
            
 
        attempFetchExpediente(withId: _login.nroDoc, withId: "", withId: "", withId: "")
        
        
        registerCell()
        expandFirstCell()
    }
    
    func addTapped(){
        print("addTapped ")
    }
    
    func timeout(){
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = .white
        print("Sesion 2\(UserDefaults.standard.getSesionTime())")
        
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Sesion 2\(UserDefaults.standard.getSesionTime())")
        
        if !UserDefaults.standard.getSesionTime(){
            navigationController?.popViewController(animated: false)
        }
    }
    
    
    func attempFetchExpediente(withId numeroDocumento: String, withId estado: String, withId fechaInicio: String, withId fechaFin: String){
        DataService.shared.requestFetchConsultarExpediente(with: numeroDocumento, with: estado, with: fechaInicio, with: fechaFin){ [weak self] (expediente, error) in
            
            if error == nil && expediente == nil{
                
                
            }
            
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let expediente = expediente else{
                return
            }
            self?.updateExpediente(with: expediente)
        }
    }
    
    func updateExpediente(with expediente: Expediente) {
        
        _expediente = expediente
        
        for modelo in _expediente{
            sectionNames.append(modelo.idExpediente)
        }
        
        for datosModelo in _expediente{
            sectionItems.append([datosModelo])
        }
        tableView.reloadData()
    }
    
}extension MyFileViewController: UITableViewDelegate, UITableViewDataSource{
    
    func registerCell() {
        let cellNib = UINib(nibName: SampleTableViewCell.reuseIdentifier, bundle: nil)
        //.register(cellNib, forCellReuseIdentifier: SampleTableViewCell.reuseIdentifier)
        tableView.register(cellNib, forCellReuseIdentifier: SampleTableViewCell.reuseIdentifier)
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 0, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
   
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SampleTableViewCell.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? SampleTableViewCell {
            cell.day.text = sectionNames[indexPath.row] as! String
            //cell.weatherIcon.image = UIImage(named: "0\(indexPath.row + 1)")
        }
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "No se ecuentraron expedientes \nascoiados a su cuenta."
            messageLabel.numberOfLines = 0;
            messageLabel.textColor = UIColor.black
            messageLabel.textAlignment = .center;
            //messageLabel.font = UIFont(name: "MuseoSans_300", size: 20.0)!
            messageLabel.sizeToFit()
            self.tableView.backgroundView = messageLabel;
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        return ""
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 200.0 : 50.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0;
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 10;
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
extension MyFileViewController: UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    
    
    func pickup(_ textField : UITextField){
        
        print("Textfield \(textField.tag)")
        
       
    }
    
    @objc func doneClick(_ sender : UITextField){
        
       
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        /*if pickerView.tag == 1{
            txtEmpresa.layer.borderWidth = 0
            if _empresa.count != 0{
                idEmpresa = _empresa[row].codEmpresa
                txtEmpresa.text = _empresa[row].empresa
            }
        }
        
        if pickerView.tag == 5{
            txtMarca.layer.borderWidth = 0
            if _marca.count != 0{
                txtMarca.text = _marca[row].descripcion
                txtModelo.text = ""
                idMarca = _marca[row].idMarca
                attempFetchModelo(marca: idMarca)
            }
        }
        
        if pickerView.tag == 6{
            txtModelo.layer.borderWidth = 0
            if _modelo.count != 0{
                idModelo = _modelo[row].idModelo
                txtModelo.text = _modelo[row].descripcion
            }
        }
        
        if pickerView.tag == 9 {
            txtFechaReporteOperador.layer.borderWidth = 0
        }
        
        if pickerView.tag == 10 {
            txtFechaReporteContacto.layer.borderWidth = 0
        }
        
        if pickerView.tag == 12{
            txtProblemaDetectado.layer.borderWidth = 0
            if _problema.count != 0{
                idProblema = Int(_problema[row].valor)!
                txtProblemaDetectado.text = _problema[row].descripcion
            }
        }*/
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        /*if  pickerView.tag == 1{
            if _empresa.count != 0{
                return _empresa[row].empresa
            }else{
                return ""
            }
        }
        
        if  pickerView.tag == 5{
            if _marca.count != 0{
                return _marca[row].descripcion
            }else{
                return ""
            }
        }
        
        if  pickerView.tag == 6{
            if _modelo.count != 0{
                return _modelo[row].descripcion
            }else{
                return ""
            }
         
        }
        
        if  pickerView.tag == 12{
            if _problema.count != 0{
                return _problema[row].descripcion
            }else{
                return ""
            }
        }*/
        
        return ""
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    */
    
}
