//
//  HomeViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/4/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController{
    
    @IBOutlet weak var OpcionesCollectionView: UICollectionView!
    var Opciones = [[:]] as [[String:AnyObject]]
    
    var encuestaModel = Encuesta()
    
    var flag_view = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIBarButtonItem(title: "PRUEBA", style: .plain, target: nil, action: nil)
    
        self.setCustomToolbar()
        
        detectScreenShot { () -> () in
            print("User took a screen shot")
        }
        
        Opciones = UserDefaults.standard.getOpciones()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        OpcionesCollectionView.reloadData()
    }

    func detectScreenShot(action: @escaping () -> ()) {
        let mainQueue = OperationQueue.main
        NotificationCenter.default.addObserver(forName: .UIApplicationUserDidTakeScreenshot, object: nil, queue: mainQueue) { notification in
            // executes after screenshot
            action()
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Opciones.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        self.setCustomToolbar()

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        let imagen = Opciones[indexPath.row]["imagen"] as! String
        cell.lblOption.text = Opciones[indexPath.row]["nombre"] as? String
        cell.imageOption.image = UIImage.init(named: imagen )
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.setCustomToolbar()

        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 2.5
        
        let numberOfItemsPerRow: CGFloat = 2.0
        let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing) / numberOfItemsPerRow
        print("itemwidth")
        print(itemWidth)
        return CGSize(width: itemWidth, height: itemWidth/1.5)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.setCustomToolbar()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let backItem = UIBarButtonItem()
        let opcionesPosicion = Opciones[indexPath.row]
        let idOpcion = Int(opcionesPosicion["id"] as! String)!
        
        var controller = storyboard.instantiateViewController(withIdentifier: "ConsultIMEIViewController")
        let controllerWebView = storyboard.instantiateViewController(withIdentifier: "ExternalWebViewController") as! ExternalWebViewController
        controller.modalPresentationStyle = .overFullScreen

        switch idOpcion {
        case 1:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_CONSULT_IMEI)
            let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
            controller.indexPosition = 1
            controller.modalPresentationStyle = .overFullScreen
            self.present(controller, animated: false, completion: nil)
            
        case 2:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_OSIPTEL_SIGNAL)
            let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
            controller.indexPosition = 3
            controller.modalPresentationStyle = .overFullScreen
            self.present(controller, animated: false, completion: nil)
            
        case 3:
            controller = storyboard.instantiateViewController(withIdentifier: "InformationViewController")
            
        case 4:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_TRASU_PROCEEDINGS)
            let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
            controller.indexPosition = 2
            controller.modalPresentationStyle = .overFullScreen
            self.present(controller, animated: false, completion: nil)
            
        case 5:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_INFORMATION_GUIDE)
            controller = storyboard.instantiateViewController(withIdentifier: "InformationViewController")
            controller.modalPresentationStyle = .overFullScreen
            
        case 6:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_VERIFY_LINE)
            controller = storyboard.instantiateViewController(withIdentifier: "VerifyViewController")
            controller.modalPresentationStyle = .overFullScreen
            
        case 7:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_OUR_OFFICES)
            let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
            controller.indexPosition = 4
            controller.modalPresentationStyle = .overFullScreen
            self.present(controller, animated: false, completion: nil)
        
        case 8:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_COMPARATEL)
            controllerWebView.urlExternal = UserDefaults.standard.getAppGlobalCompTel()
            controllerWebView.modalPresentationStyle = .fullScreen
            flag_view = true
            self.present(controllerWebView, animated: true, completion: nil)
            
        case 9:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_COMPARAMOVIL)
            controllerWebView.urlExternal = UserDefaults.standard.getAppGlobalCompMov()
            controller.modalPresentationStyle = .fullScreen
            flag_view = true
            self.present(controllerWebView, animated: true, completion: nil)
            
        case 10:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_PUNKU)
            controllerWebView.urlExternal = UserDefaults.standard.getAppGlobalPunku()
            controllerWebView.modalPresentationStyle = .fullScreen
            flag_view = true
            self.present(controllerWebView, animated: true, completion: nil)
            
        default:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_ENTER_RATE_US)
            controller = storyboard.instantiateViewController(withIdentifier: "RateUsViewController")
            controller.modalPresentationStyle = .overFullScreen
        }
        
        if !flag_view {
            backItem.title = " "
            navigationItem.backBarButtonItem = backItem
            controller.modalPresentationStyle = .overFullScreen
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            flag_view = false
        }
    }
    
    func attempFetchCreateEventoLog(withId accion: String){
        
        let sistemaOperativo = "iOS"
        let marcaCelular = "Apple"
        let modeloCelular = UIDevice.current.modelName
        let MAC = UserDefaults.standard.getUUID()
        var nombreUsuario = ""
        var numeroCelular = ""
        if(UserDefaults.standard.getNameComplete() != nil){
            nombreUsuario = UserDefaults.standard.getUserID()
            numeroCelular = UserDefaults.standard.getNumber()
        }
        
        let PostB = BodyEntityLog.init(sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, accion: accion, mac: MAC, nombreUsuario: nombreUsuario)
        
        DataService.shared.requestFetchCreateEventoLog(with: PostB) {[weak self] (trasu, error, String) in
            if error != nil {
                print("error")
                print(error)
            }
            
            if trasu == "Ok"{
                print("CreateEventoLog OK")
            }else if trasu == "error"{
               print("CreateEventoLog ERROR")
            }
        }
    }
    
    func setCustomToolbar(){
        let item = UIBarButtonItem(image: UIImage(named: "ic_information_home")?.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = item
        
        navigationController?.navigationBar.barTintColor = UIColor.white

        let logoImage = UIImage.init(named: "osiptel_logo")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x:0.0,y:0.0, width:150,height:50.0)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 150)
        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 50)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        navigationItem.leftBarButtonItem = imageItem
    }
    
}
