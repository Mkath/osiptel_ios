//
//  ViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 29/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RegisterUserViewController: UIViewController, UITextFieldDelegate,NVActivityIndicatorViewable{

    var ResponseUsuarioModel:ResponseUsuario?
    var _opciones = [[:]] as [[String:AnyObject]]
    var isValidate = true
    private var PickerTipoDocumento : UIPickerView?
    var _tipoDocumento  : [String] = ["Seleccionar","DNI"]

    @IBOutlet weak var lblValidate: UILabel!
    @IBOutlet weak var imageValidate: UIImageView!
    
    @IBOutlet var viewMessage: UIView!
    
    @IBAction func BackButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    //Campos de texto
    @IBOutlet weak var txtCell: UITextField!// Cellphone
    @IBOutlet weak var lblCellValidate: UILabel!
    @IBOutlet weak var txtDocumentNumber: UITextField!
    @IBOutlet weak var lblDocumentNumberValidate: UILabel!
    @IBOutlet weak var txtDocumentType: UITextField!
    
    @IBOutlet weak var lblDocumentTypeValidate: UILabel!
    @IBOutlet weak var txtEmail: UITextField! //Email
    @IBOutlet weak var lblEmailValidate: UILabel!
    @IBOutlet weak var txtName: UITextField! // Nombre
    @IBOutlet weak var lblNameValidate: UILabel!
    @IBOutlet weak var txtLastNameOne: UITextField! //Apellido Pa
    @IBOutlet weak var lblLastPValidate: UILabel!
    @IBOutlet weak var txtLastNameTwo: UITextField! // Apellido Ma
    //Boton Siguiente
    @IBOutlet weak var lblLastMValidate: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    var visualEffect:UIVisualEffect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NoticacionCenter Movimiento de vista con respecto al teclado
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        txtCell.delegate = self
        txtCell.tag = 0
        txtEmail.delegate = self
        txtEmail.tag = 1
        txtName.delegate = self
        txtName.tag = 2
        txtLastNameOne.delegate = self
        txtLastNameOne.tag = 3
        txtLastNameTwo.delegate = self
        txtLastNameTwo.tag = 4
        
        lblCellValidate.alpha = 0
        lblNameValidate.alpha = 0
        lblEmailValidate.alpha = 0
        lblLastMValidate.alpha = 0
        lblLastPValidate.alpha = 0
        lblDocumentTypeValidate.alpha = 0
        lblDocumentNumberValidate.alpha = 0
        
        PickerTipoDocumento = UIPickerView()
        PickerTipoDocumento?.delegate = self
        PickerTipoDocumento?.dataSource = self
        PickerTipoDocumento?.tag = 0
        txtDocumentType.inputView = PickerTipoDocumento

    }
    
    
    func attempFetchUsuarioPost(withId post: PostUsuario){
        
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        DataService.shared.requestFetchCreateAccountUser(with: post) {[weak self] (usuario, error) in
            
            self?._opciones = UserDefaults.standard.getOpciones()
            if let error = error {
                self?.stopAnimating()
                self!.alertViewController(titulo: "", mensaje: "No se pudo realizar el pre-registro", closeFlag: true, imageValidate: false, closeAutomatic: false , sendVC: "CustomTabBarView")
                return
            }
     
                let idUser = UserDefaults.standard.getUserID()
                
                DataService.shared.requestFetchCalificanos(with: idUser) { [weak self] (encuesta, error) in
                    self?.stopAnimating()
                    
                    if let error = error {
                        print("error \(error.localizedDescription)")
                        
                    }
                    guard let encuesta = encuesta else{
                        return
                    }
                    
                    if encuesta.first?.idUsuario != nil{
                        self?._opciones.append(["id":"11","nombre":"Califícanos","imagen":"ic_calificanos"] as! [String : AnyObject])
                    }
                    
                    UserDefaults.standard.setOpciones(value: self!._opciones)
                    UserDefaults.standard.setLoggedIn(value: true)

                    self!.alertViewController(titulo: "", mensaje: "Se ha registrado con éxito", closeFlag: false, imageValidate: true, closeAutomatic: true, sendVC: "CustomTabBarView")
                }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 0{
            lblDocumentTypeValidate.alpha = 0
        }
        
        if textField.tag == 1{
            lblDocumentNumberValidate.alpha = 0
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 2{
            lblCellValidate.alpha = 0
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 3{
            lblEmailValidate.alpha = 0
        }
        
        if textField.tag == 4{
            lblNameValidate.alpha = 0
        }
        
        if textField.tag == 5{
            lblLastPValidate.alpha = 0
        }
        
        if textField.tag == 6{
            lblLastMValidate.alpha = 0
        }
        
        
        if textField.tag == 4 || textField.tag == 5 || textField.tag == 6 {
           
            let allowedCharacters = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
            
        }
        

        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            returnValue = false
        }
        
        return  returnValue
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if self.view.frame.origin.y < 0 {
            self.view.frame.origin.y -=  self.view.frame.origin.y
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            print(keyboardHeight)
            self.view.frame.origin.y -= keyboardHeight/2
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            print(keyboardHeight)
            self.view.frame.origin.y += keyboardHeight/2
        }
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    

    
    @IBAction func NextBtnAction(_ sender: Any) {
        
        let IdDevice = UserDefaults.standard.getUUID()
        let ModelDevice = UIDevice.current.modelName as! String
            UserDefaults.standard.setModelDevice(value: ModelDevice)
        let _usuarioRegistro = "\(txtName.text!) \(txtLastNameOne.text!) \(txtLastNameTwo.text!)"

        let PostU = PostUsuario.init(tipoDocumento: txtDocumentType.text!, numeroDocumento: txtDocumentNumber.text!, nroTelefono: txtCell.text!, correoElectronico: txtEmail.text!, nombre: txtName.text!, apellidoPaterno: txtLastNameOne.text!, apellidoMaterno: txtLastNameTwo.text!, modeloCelular: ModelDevice, marcaCelular: "apple", mac: IdDevice, usuarioRegistro: _usuarioRegistro)
        txtEmail.text!.condenseWhitespace()
        txtName.text = txtName.text!.condenseWhitespace()
        txtLastNameOne.text = txtLastNameOne.text!.condenseWhitespace()
        txtLastNameTwo.text = txtLastNameTwo.text!.condenseWhitespace()

        isValidate = true
        
        if !isValidEmailAddress(emailAddressString: txtEmail.text!){
            isValidate = false
            lblEmailValidate.text = "Ingrese un correo electronico valido"
            lblEmailValidate.alpha = 1
        }
        
        if txtEmail.text == " "{
            isValidate = false
            lblEmailValidate.text = "Ingrese su correo electronico"
            lblEmailValidate.alpha = 1
        }
        
        if txtEmail.text?.count == 0{
            isValidate = false
            lblEmailValidate.text = "Ingrese su correo electronico"
            lblEmailValidate.alpha = 1
        }
        
        if txtName.text?.count == 0{
            isValidate = false
            lblNameValidate.text = "Ingrese su nombre"
            lblNameValidate.alpha = 1
        }
        
        if txtLastNameOne.text == " "{
            isValidate = false
            lblLastPValidate.text = "Ingrese su apellido paterno"
            lblLastPValidate.alpha = 1
        }
        
        if txtLastNameOne.text?.count == 0{
            isValidate = false
            lblLastPValidate.text = "Ingrese su apellido paterno"
            lblLastPValidate.alpha = 1
        }
        
        if txtLastNameTwo.text == " "{
            isValidate = false
            lblLastMValidate.text = "Ingrese su apellido materno"
            lblLastMValidate.alpha = 1
        }
        
        if txtLastNameTwo.text?.count == 0{
            isValidate = false
            lblLastMValidate.text = "Ingrese su apellido materno"
            lblLastMValidate.alpha = 1
        }
        
        if (txtCell.text?.count)! < 9{
            isValidate = false
            lblCellValidate.text = "Ingrese un numero de celular de 9 digitos"
            lblCellValidate.alpha = 1
        }
        
        if  txtDocumentNumber.text?.count == 0{
            isValidate = false
            lblDocumentNumberValidate.text = "Ingrese su dni"
            lblDocumentNumberValidate.alpha = 1
        }
        
        if (txtDocumentNumber.text?.count)! < 8{
            isValidate = false
            lblDocumentNumberValidate.text = "Ingrese un dni de 8 digitos"
            lblDocumentNumberValidate.alpha = 1
        }
        
        if  txtDocumentType.text?.count != 3{
            isValidate = false
            lblDocumentTypeValidate.text = "Ingrese un tipo de documento"
            lblDocumentTypeValidate.alpha = 1
        }
        
        if isValidate{
            attempFetchUsuarioPost(withId: PostU)
        }
    }
    

}


extension String {
    func condenseWhitespace() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}


extension RegisterUserViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if  pickerView.tag == 0{
            return 2
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0{
            txtDocumentType.text =  _tipoDocumento[row]
            txtDocumentNumber.text = ""
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if  pickerView.tag == 0{
            return _tipoDocumento[row]
        }
    
        
        return ""
    }
}


