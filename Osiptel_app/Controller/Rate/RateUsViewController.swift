//
//  RateUsViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class RateUsViewController: UIViewController {

    @IBOutlet weak var tableRateUs: UITableView!
    var _pregunta = Pregunta()
    var _postPregunta: PostPregunta!
    var _usuarioPregunta: UsuarioPregunta!
    var _postP =  [UsuarioPregunta]()
    var parameteresItem : [[String:AnyObject]] = []
    var isValidate = true
    var _opciones = [[:]] as [[String:AnyObject]]
    
    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = String(UserDefaults.standard.getUserID())
    let idUsuario = Int(UserDefaults.standard.getUserID())
    let usuarioRegistro = String(UserDefaults.standard.getUserID())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ayudanos a mejorar"
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        attempFetchPregunta()
    }

    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    

    func attempFetchPregunta(){
        DataService.shared.requestFetchPreguntas{ [weak self] (pregunta, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let pregunta = pregunta else{
                return
            }
            self?.updatePreguntas(with: pregunta)
        }
    }
    
    func updatePreguntas(with pregunta: Pregunta) {
        
        _pregunta = pregunta
        tableRateUs.reloadData()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableRateUs.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tableRateUs.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }

}

extension RateUsViewController: UITableViewDelegate, UITableViewDataSource, UITextViewDelegate , UITextFieldDelegate, RateUsTableViewCellDelegate, RatingStartTableViewCellDelegate{
    
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let indexPath = IndexPath(row: parameteresItem.count - 2, section: 0)
        let cell = tableRateUs.cellForRow(at: indexPath) as? ComentaryTableViewCell
        let comentaryText = cell?.txtComentary.text
        cell?.txtComentary.layer.borderWidth = 1
        cell?.txtComentary.borderColorView = UIColor(red:0.03, green:0.27, blue:0.30, alpha:1.0)
        return true
        
    }
    
    func RatingStartViewCellSend(idPregunta: Int, respuesta: String) {
        
        isValidate = true
        _postP.removeAll()

        let indexPath = IndexPath(row: parameteresItem.count - 2, section: 0)
        let cell = tableRateUs.cellForRow(at: indexPath) as? ComentaryTableViewCell
        let comentaryText = cell?.txtComentary.text
        
        if comentaryText!.isEmpty{
            isValidate = false
            cell?.txtComentary.layer.cornerRadius = 4
            cell?.txtComentary.borderColorView = UIColor.red
            cell?.txtComentary.layer.borderWidth = 1.0
        }else{
            let idvaluec = parameteresItem[parameteresItem.count-2]["idPregunta"] as! Int
            if let Comentary = self.parameteresItem.firstIndex(where: {$0["idPregunta"] as! Int == idvaluec}){
                parameteresItem[Comentary]["valor"] = comentaryText  as AnyObject
            }
        }
        let idvaluer = parameteresItem[parameteresItem.count-1]["idPregunta"] as! Int
        if let Rating = self.parameteresItem.firstIndex(where: {$0["idPregunta"] as! Int == idvaluer}){
            parameteresItem[Rating]["valor"] = respuesta  as! AnyObject
            
        }
        
        
        for Preguntas in parameteresItem{
            
            let idPregunta = Preguntas["idPregunta"] as! Int
            let valor = Preguntas["valor"] as! String
            
            _postP.append(UsuarioPregunta(idPregunta: idPregunta, valor: valor))
            if Preguntas["valor"] as! String == ""{
                if Preguntas["valor"]  as! String !=  "" {
                    isValidate = false
                }
            }
        }
        
        if isValidate{
            attempFetchUsuarioPost(withId: _postP)
        }
     
    }
    

    func RateUsTableViewCellDidTapHeart(idPregunta: Int, respuesta: String) {
        
        if let row = self.parameteresItem.firstIndex(where: {$0["idPregunta"] as! Int == idPregunta}){
            parameteresItem[row]["valor"] = respuesta  as! AnyObject
        }
    }
    
    func RateUsTableViewCellDidTapTrash(idPregunta: Int, respuesta: String) {

    }
    
    func attempFetchUsuarioPost(withId post: [UsuarioPregunta]){
        
        let _postPregunta = PostPregunta.init(idUsuario: idUsuario!, usuarioRegistro: usuarioRegistro, usuarioPregunta: post, sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, mac: MAC, nombreUsuario: nombreUsuario)
        
        print(_postPregunta)
        
        DataService.shared.requestFetchSendQuestion(with: _postPregunta) {[weak self] (pregunta, error) in
            if let error = error {
                print("UsuarioPost \(error)")
                self!.alertViewController(titulo: "", mensaje: "Error al enviar su encuesta, intente nuevamente porfavor", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                return
            }
            if pregunta == nil{
                self!.updateUsuarioPost()
            }
        }
    }
    
    func updateUsuarioPost() {
        
        _opciones = [["id":"1","nombre":"Consulta IMEI","imagen":"ic_consulta_IMEI"],["id":"2","nombre":"Señal OSIPTEL","imagen":"ic_senal_osiptel"],["id":"4","nombre":"Expedientes Trasu","imagen":"ic_expedientes"],["id":"5","nombre":"Guía de Información","imagen":"ic_guia_informativa"],["id":"6","nombre":"Verifica tu línea","imagen":"ic_verifica_linea"],["id":"7","nombre":"Nuestras oficinas","imagen":"ic_nuestras"],["id":"8","nombre":"Comparatel","imagen":"ic_comptel"],["id":"9","nombre":"Comparamóvil","imagen":"ic_comparamovil"],["id":"10","nombre":"Punku","imagen":"ic_punku"]] as [[String:AnyObject]]
        
        UserDefaults.standard.setCalificanos(value: true)
        UserDefaults.standard.setOpciones(value: _opciones)
        
        alertViewController(titulo: "", mensaje: "Su encuesta ha sido enviada con éxito", closeFlag: false, imageValidate: true, closeAutomatic: true, sendVC: "CustomTabBarView")

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _pregunta.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == _pregunta.count - 1{
            return 140
            
        }else if indexPath.row == _pregunta.count - 2{
            return 160
            
        }else {
            return 120
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell

        
        let preguntas = _pregunta[indexPath.row]
        
            if indexPath.row == _pregunta.count - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell", for: indexPath) as! RatingStartTableViewCell
                
                cell.contentView.isExclusiveTouch = true
                cell.isExclusiveTouch = true
                
                cell.starBtn.isExclusiveTouch = true

                cell.isUserInteractionEnabled = true
                
                cell.setRate(pregunta: preguntas)
                cell.delegate = self
                if parameteresItem.count < _pregunta.count{
                    parameteresItem.append(["idPregunta":preguntas.idPregunta, "valor":"0"] as [String : AnyObject])
                }
                return cell
                
            }else if indexPath.row == _pregunta.count - 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ComentaryCell", for: indexPath) as! ComentaryTableViewCell
                cell.setRate(pregunta: preguntas)
                cell.txtComentary.delegate = self
                if parameteresItem.count < _pregunta.count{
                    parameteresItem.append(["idPregunta":preguntas.idPregunta, "valor":""] as [String : AnyObject])
                }
                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell", for: indexPath) as! RateUsTableViewCell
                cell.setRate(pregunta: preguntas)
                cell.delegate = self
                if parameteresItem.count < _pregunta.count{
                    parameteresItem.append(["idPregunta":preguntas.idPregunta, "valor":""] as [String : AnyObject])
                }
                return cell
            }
    }
}
