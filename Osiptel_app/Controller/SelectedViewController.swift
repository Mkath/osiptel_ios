//
//  SeleccionarViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/4/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class SelectedViewController: UIViewController {

    override func viewDidLoad() {

        super.viewDidLoad()

    }
    
    @IBAction func btnIngresar(_ sender: Any) {
        
        let sistemaOperativo = "iOS"
        let marcaCelular = "Apple"
        let modeloCelular = UIDevice.current.modelName as! String
        let accion = global.ACTION_GETIN_APP
        let MAC = UserDefaults.standard.getUUID()
        var nombreUsuario = ""
        var numeroCelular = ""
        if(UserDefaults.standard.getNameComplete() != nil){
            nombreUsuario = UserDefaults.standard.getUserID()
            numeroCelular = UserDefaults.standard.getNumber()
        }
        
        let PostB = BodyEntityLog.init(sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, accion: accion, mac: MAC, nombreUsuario: nombreUsuario)
        attempFetchCreateEventoLog(withId: PostB)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView")
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func btnRegistrar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RegisterUserViewController") as! RegisterUserViewController
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
    func attempFetchCreateEventoLog(withId post: BodyEntityLog){
        
        DataService.shared.requestFetchCreateEventoLog(with: post) {[weak self] (trasu, error, String) in
            if error != nil {
                print("error")
                print(error)
            }
            
            if trasu == "Ok"{
                print("CreateEventoLog OK")
            }else if trasu == "error"{
               print("CreateEventoLog ERROR")
            }
        }
    }
}
