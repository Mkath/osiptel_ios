//
//  ComentaryViewController.swift
//  Osiptel_app
//
//  Created by GYS on 3/28/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class ComentaryViewController: UIViewController {

    @IBOutlet weak var txtViewController: UITextView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var titleview: UIView!
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var btnClose: UIButton!
    var visual:UIVisualEffect!
    var comentario = ""
    @IBAction func btnCloseAction(_ sender: Any) {
        dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visual = visualEffect.effect
        visualEffect.effect = nil
        btnClose.alpha = 1
        titleview.roundedCorners(top: true)
        
        txtViewController.text = comentario
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
        tapGesture.cancelsTouchesInView = true
        visualEffect.addGestureRecognizer(tapGesture)
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        viewAlert.center = visualEffect.center
        viewAlert.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewAlert.alpha = 0
        
        UIView.animate(withDuration: 0.3) {
            self.visualEffect.effect = self.visual
            self.visualEffect.alpha = 1
            self.viewAlert.alpha = 1
            self.viewAlert.transform = CGAffineTransform.identity
        }
    }
    
    @objc func dismissModal(_ sender: UITapGestureRecognizer){
        dismiss()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.visualEffect.effect = nil
            self.viewAlert.alpha = 0
            self.visualEffect.alpha = 0
            
        }) { (success:Bool) in
            self.dismiss(animated: true, completion: nil)
        }
        
    }

}
