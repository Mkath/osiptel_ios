    //
//  SignalcoverageViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import EasyTipView

    
class SignalcoverageViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapSignalView: MKMapView!
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    @IBOutlet weak var viewLocatorButton: UIView!
    @IBOutlet weak var btnConsultar: UIButton!
    var timer:Timer?
    var timeLeft = 4
    var tipView :EasyTipView?
    var isFirst : Bool = true
    
    @IBOutlet weak var btnReportar: UIButton!
    @IBAction func btnShareAction(_ sender: Any) {
        

        let someText:String = ""
        let objectsToShare:URL = URL(string: UserDefaults.standard.getAppGlobalEnlaceCob())!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view

        
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivityType.openInIBooks, UIActivityType.print, UIActivityType.addToReadingList,  UIActivityType.postToVimeo, UIActivityType.assignToContact]
        
        activityViewController.navigationController?.navigationBar.tintColor = UIColor.white
        activityViewController.navigationController?.navigationBar.barTintColor = UIColor.white        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)

    }
    
    @objc private final func tapOnLocation(_ tapGesture: UITapGestureRecognizer){
        centerViewOnUserLocation()
    }
    
    @IBAction func btnConsultarAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FilterCoverageViewController") as! FilterCoverageViewController
        
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        controller.navigationItem.title = "Consultar Cobertura"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnReportAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportCoverageViewController") as! ReportCoverageViewController
        
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        controller.navigationItem.title = "Reportar"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    //let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnLocation(_:)))
        viewLocatorButton.addGestureRecognizer(tapGesture)
        mapSignalView.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Check for Location Services
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = #colorLiteral(red: 1, green: 0.5329053591, blue: 0.2610576485, alpha: 1)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        
        EasyTipView.globalPreferences = preferences
        
        tipView = EasyTipView(text: "Consulta aquí la cobertura de cada operador de telefonía.", preferences: preferences)
        
        tipView?.show(forView: btnConsultar)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)

    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        if timeLeft <= 0 {
            timer?.invalidate()
            timer = nil
            
            if isFirst {
                tipView?.dismiss()
                
                timeLeft = 5
                
                var preferences = EasyTipView.Preferences()
                preferences.drawing.foregroundColor = UIColor.white
                preferences.drawing.backgroundColor = #colorLiteral(red: 1, green: 0.5329053591, blue: 0.2610576485, alpha: 1)
                preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
                
                
                tipView = EasyTipView(text: "Reporta aquí si algún operador no cumple con las normas.", preferences: preferences)
                
                tipView?.show(forView: btnReportar)
                
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
                
                isFirst = false
                
            }else{
                tipView?.dismiss()
            }
            

        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation == nil {
            // Zoom to user location
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
                mapSignalView.setRegion(viewRegion, animated: false)
            }
        }
    }

    func centerViewOnUserLocation(){
        if let location = locationManager.location?.coordinate{
            let span = MKCoordinateSpan.init(latitudeDelta: 0.0075, longitudeDelta: 0.0075)
            let region = MKCoordinateRegion.init(center: location, span: span)
            mapSignalView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func BtnFilterCoverage(_ sender: Any) {
        //Llamando a vista filtro
        //let FilterVC = FilterCoverageViewController()
        //FilterVC.modalPresentationStyle = .overCurrentContext
        //present(FilterVC, animated: true, completion: nil)

    }
}
