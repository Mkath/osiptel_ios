//
//  PlaceholderColorExtension.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}

