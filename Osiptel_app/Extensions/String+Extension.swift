//
//  String+Extension.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/6/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

extension String {
    
    static func replaceHttpToHttps(with urlString: String) -> String? {
        // split into array
        let separateCriteria = "://"
        var parts = urlString.components(separatedBy: separateCriteria)
        // do checking and join
        for (index, part) in parts.enumerated() {
            print("\(index) : \(part)")
        }
        parts[0] = "https"
        guard let firstpart = parts.first, let lastPart = parts.last else {
            print("AFString+Extension: replaceHttpToHttps: failed")
            return nil
        }
        let finalString = firstpart + separateCriteria + lastPart
        return finalString
    }
}
