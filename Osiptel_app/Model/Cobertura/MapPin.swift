//
//  CustomAnnotation.swift
//  Osiptel_app
//
//  Created by GYS on 4/10/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class MapPin: MKPointAnnotation{
    var oficina: String?
    var direccion: String?
    var horarioLV: String?
    var horarioS: String?
    var horarioD: String?
    init(oficina: String?, direccion: String?, horarioLV: String?, horarioS: String?, horarioD: String?){
        self.oficina = oficina
        self.direccion = direccion
        self.horarioLV = horarioLV
        self.horarioS = horarioS
        self.horarioD = horarioD
    }
    
}
