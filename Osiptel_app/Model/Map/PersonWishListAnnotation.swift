//
//  PersonWishListAnnotation.swift
//  CustomPinsMap
//
//  Created by Ignacio Nieto Carvajal on 6/12/16.
//  Copyright © 2016 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit
import MapKit

class PersonWishListAnnotation: NSObject, MKAnnotation {
    var person: Person
    var oficina: OficinaElement
    var coordinate: CLLocationCoordinate2D { return person.location }
    
    init(person: Person, oficina: OficinaElement) {
        self.person = person
        self.oficina = oficina
        super.init()
    }
    
    var title: String? {
        //return person.name
        return oficina.nombreOficina
    }
    
    /*var subtitle: String? {
        //return person.wishList.joined(separator: ", ")
    }*/
    
}
