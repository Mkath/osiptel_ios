//
//  Operadores.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/20/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias Operador = [OperadorElement]

struct OperadorElement: Codable {
    let idOperador: Int?
    let nombreComercial, razonSocial: String
    let enlace: String
    let nombreImagen: String?
    let codigoOperador: String
}

// MARK: Convenience initializers and mutators

extension OperadorElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(OperadorElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        idOperador: Int? = nil,
        nombreComercial: String? = nil,
        razonSocial: String? = nil,
        enlace: String? = nil,
        nombreImagen: String?? = nil,
        codigoOperador: String? = nil
        ) -> OperadorElement {
        return OperadorElement(
            idOperador: idOperador ?? self.idOperador,
            nombreComercial: nombreComercial ?? self.nombreComercial,
            razonSocial: razonSocial ?? self.razonSocial,
            enlace: enlace ?? self.enlace,
            nombreImagen: nombreImagen ?? self.nombreImagen,
            codigoOperador: codigoOperador ?? self.codigoOperador
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Operador.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Operador.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseOperador(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Operador>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
