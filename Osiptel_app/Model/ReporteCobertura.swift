//
//  ReporteCobertura.swift
//  Osiptel_app
//
//  Created by GYS on 3/19/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias ReporteCobertura = [ReporteCoberturaElement]

struct ReporteCoberturaElement: Codable {
    let numEncuesta, nombre, empresa, recLlam: String?
    let reaLlam, comentario, idCalidad, nivCob: String?
    let marcaEquipo, modelo, so, correo: String?
    let fechaVisita, fechaRegistro, lugar: String?
}

// MARK: Convenience initializers and mutators

extension ReporteCoberturaElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ReporteCoberturaElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        numEncuesta: String? = nil,
        nombre: String? = nil,
        empresa: String? = nil,
        recLlam: String? = nil,
        reaLlam: String? = nil,
        comentario: String? = nil,
        idCalidad: String? = nil,
        nivCob: String? = nil,
        marcaEquipo: String? = nil,
        modelo: String? = nil,
        so: String? = nil,
        correo: String? = nil,
        fechaVisita: String? = nil,
        fechaRegistro: String? = nil,
        lugar: String? = nil
        ) -> ReporteCoberturaElement {
        return ReporteCoberturaElement(
            numEncuesta: numEncuesta ?? self.numEncuesta,
            nombre: nombre ?? self.nombre,
            empresa: empresa ?? self.empresa,
            recLlam: recLlam ?? self.recLlam,
            reaLlam: reaLlam ?? self.reaLlam,
            comentario: comentario ?? self.comentario,
            idCalidad: idCalidad ?? self.idCalidad,
            nivCob: nivCob ?? self.nivCob,
            marcaEquipo: marcaEquipo ?? self.marcaEquipo,
            modelo: modelo ?? self.modelo,
            so: so ?? self.so,
            correo: correo ?? self.correo,
            fechaVisita: fechaVisita ?? self.fechaVisita,
            fechaRegistro: fechaRegistro ?? self.fechaRegistro,
            lugar: lugar ?? self.lugar
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == ReporteCobertura.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ReporteCobertura.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseReporteCobertura(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ReporteCobertura>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
