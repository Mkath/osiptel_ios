//
//  Expedientes.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/12/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias Expediente = [ExpedienteElement]

struct ExpedienteElement: Codable {
    let idExpediente: String
    let recurso, estado, fechaIngreso, nroReclamo: String
    let empresa: String
}

// MARK: Convenience initializers and mutators

extension ExpedienteElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ExpedienteElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        idExpediente: String? = nil,
        recurso: String? = nil,
        estado: String? = nil,
        fechaIngreso: String? = nil,
        nroReclamo: String? = nil,
        empresa: String? = nil
        ) -> ExpedienteElement {
        return ExpedienteElement(
            idExpediente: idExpediente ?? self.idExpediente,
            recurso: recurso ?? self.recurso,
            estado: estado ?? self.estado,
            fechaIngreso: fechaIngreso ?? self.fechaIngreso,
            nroReclamo: nroReclamo ?? self.nroReclamo,
            empresa: empresa ?? self.empresa
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Expediente.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Expediente.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseExpediente(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Expediente>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
