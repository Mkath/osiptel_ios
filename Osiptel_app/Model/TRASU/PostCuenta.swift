//
//  PostCuenta.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/17/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct PostCuenta: Codable {
    let tipoDocumento, numeroDocumento, nombres, apellidoPaterno: String
    let apellidoMaterno, correoElectronico, confirmarCorreoElectronico: String
    let idServicioReclamado, idCodigoAsociado: Int
    let codigoReclamo, contrasenia, confirmarContrasenia, sistemaOperativo: String
    let marcaCelular, modeloCelular, numeroCelular, mac: String
    let nombreUsuario: String
}

// MARK: Convenience initializers and mutators

extension PostCuenta {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PostCuenta.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        tipoDocumento: String? = nil,
        numeroDocumento: String? = nil,
        nombres: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        correoElectronico: String? = nil,
        confirmarCorreoElectronico: String? = nil,
        idServicioReclamado: Int? = nil,
        idCodigoAsociado: Int? = nil,
        codigoReclamo: String? = nil,
        contrasenia: String? = nil,
        confirmarContrasenia: String? = nil,
        sistemaOperativo: String? = nil,
        marcaCelular: String? = nil,
        modeloCelular: String? = nil,
        numeroCelular: String? = nil,
        mac: String? = nil,
        nombreUsuario: String? = nil
        ) -> PostCuenta {
        return PostCuenta(
            tipoDocumento: tipoDocumento ?? self.tipoDocumento,
            numeroDocumento: numeroDocumento ?? self.numeroDocumento,
            nombres: nombres ?? self.nombres,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            confirmarCorreoElectronico: confirmarCorreoElectronico ?? self.confirmarCorreoElectronico,
            idServicioReclamado: idServicioReclamado ?? self.idServicioReclamado,
            idCodigoAsociado: idCodigoAsociado ?? self.idCodigoAsociado,
            codigoReclamo: codigoReclamo ?? self.codigoReclamo,
            contrasenia: contrasenia ?? self.contrasenia,
            confirmarContrasenia: confirmarContrasenia ?? self.confirmarContrasenia,
            sistemaOperativo: sistemaOperativo ?? self.sistemaOperativo,
            marcaCelular: marcaCelular ?? self.marcaCelular,
            modeloCelular: modeloCelular ?? self.modeloCelular,
            numeroCelular: numeroCelular ?? self.numeroCelular,
            mac: mac ?? self.mac,
            nombreUsuario: nombreUsuario ?? self.nombreUsuario
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostCuenta(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PostCuenta>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
