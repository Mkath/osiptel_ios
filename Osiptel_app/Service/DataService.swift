//
//  DataService.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/6/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct DataService {
    
    // MARK: - Singleton    
    static let shared = DataService()
    var _usuario = Usuario()
    // MARK: - URL
    private var endPoint = "http://serviciostest.osiptel.gob.pe:81/OsiptelAppAPI/api/v1/"
    private var cobertura = "http://serviciostest.osiptel.gob.pe:81/OsiptelAppCoberturaMovil/api/v1/"
    private var sigem = "http://serviciostest.osiptel.gob.pe:81/OsiptelAppSigem/api/v1/"
    private var trasu = "http://serviciostest.osiptel.gob.pe:81/OsiptelAppTrasu/api/v1/"
    private var tokenAccess = "http://serviciostest.osiptel.gob.pe:81/OsiptelAppIdentityServer/connect/token"
    
//    private var endPoint = "https://operacionesweb.osiptel.gob.pe/OsiptelAppAPI/api/v1/"
//    private var cobertura = "https://operacionesweb.osiptel.gob.pe/OsiptelAppCoberturaMovil/api/v1/"
//    private var sigem = "https://operacionesweb.osiptel.gob.pe/OsiptelAppSigem/api/v1/"
//    private var trasu = "https://operacionesweb.osiptel.gob.pe/OsiptelAppTrasu/api/v1/"
//    private var tokenAccess = "https://identidadweb.osiptel.gob.pe/OsiptelAppIdentityServer/connect/token"

    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = UserDefaults.standard.getNameComplete()
    
    //GET TOKEN
    //completion: @escaping (Error?) -> ()
    func requestToken(){
        let body = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials"

        let url = URL(string: tokenAccess)
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = body.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { response in
                if response.result.value != nil{
                    if response.response?.statusCode == 200{
                        let JsonData = JSON(response.result.value!)
                        let token = "\(JsonData["access_token"])"
                        let tokenType = "\(JsonData["token_type"])"
                        UserDefaults.standard.setToken(value: token)
                        UserDefaults.standard.setTypeToken(value: tokenType)
                        UserDefaults.standard.set(Date(), forKey: "tokenAcquisitionTime")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tokenAcquired"), object: nil)
                    }
                    if response.response?.statusCode == 500{
                    }
                    //completion(nil)
                }
            
            if response.error != nil {
                let error = response.error
                //completion(error)
                return
            }
        }        
    }
    
    func refreshRequestToken(completion: @escaping (String?,Error?) -> ()){
        let body = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials"
        //Alamofire.request(tokenAccess, method: .post, parameters: [:], encoding: body, headers: [:])
        let url = URL(string: tokenAccess)
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = body.data(using: .utf8)
       
        Alamofire.request(request)
            .responseJSON { response in
                if let data = response.result.value{
                    let JsonData = JSON(response.result.value!)
                    let token = "\(JsonData["access_token"])" as! String
                    let tokenType = "\(JsonData["token_type"])" as! String
                    print("token \(token)")
                    print("tokenType \(tokenType)")
                    UserDefaults.standard.setToken(value: token)
                    UserDefaults.standard.setTypeToken(value: tokenType)
                    completion("Success", nil)
                }
                
        }
    }

    
    //GetCalificanosByTemporada
    func requestFetchCalificanos(with user: String,completion: @escaping (Encuesta?, Error?) -> ()){
        let url = "\(endPoint)Encuesta/GetCalificanosByTemporada?IdUsuario=\(user)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]

        Alamofire.request(url,headers: headers).responseEncuesta { response in
            
            if response.response?.statusCode == 401{
                print("Error 401")
                completion(nil, nil)
                return
            }
            
            if let error = response.error {
                print("Error calificanos \(error)")
                completion(nil, error)
                return
            }
            
            if let encuesta = response.result.value {
                completion(encuesta, nil)
                return
            }
        }
    }
    
    //GetUserDataByMac
    func requestFetchUserData(with mac: String,completion: @escaping (Usuario?, Error?) -> ()){
        let url = "\(endPoint)Usuario/GetUserDataByMac?mac=\(mac)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        Alamofire.request(url,headers: headers).responseUsuario { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let usuario = response.result.value {
                completion(usuario, nil)
                return
            }
        }
    }
    
    //PostUser
    func requestFetchCreateAccountUser(with usuarioPost: PostUsuario,completion: @escaping (ResponseUsuario?, Error?) -> ()){
        let urlString = "\(endPoint)Usuario/CreateCountUser"
        let decoder = try? JSONEncoder().encode(usuarioPost.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON{ response in
                //Response
                if let error = response.error {
                    completion(nil, error)
                    return
                }
            if response.result.value != nil {
                    if response.response?.statusCode == 200{
                        let swiftyJsonVar = JSON(response.result.value!)
                        let jsonDecoder = JSONDecoder()
                        do {
                            let UserData = try jsonDecoder.decode( Usuario.self, from: response.data!)
                            let Userid = String(UserData.first!.idUsuario)
                            let UserdocumentType = UserData.first!.tipoDocumento
                            let nroDocumento = UserData.first!.nroDocumento
                            let nombre = UserData.first!.nombre
                            let apellidoP = UserData.first!.apellidoPaterno
                            let apellidoM = UserData.first!.apellidoMaterno
                            let numero = UserData.first!.nroTelefono
                            let username = "\(UserData.first!.nombre) \(UserData.first!.apellidoPaterno) \(UserData.first!.apellidoMaterno)"
                            let email = UserData.first?.correoElectronico
                            
                            UserDefaults.standard.setUserID(value: Userid)
                            UserDefaults.standard.setName(value: nombre)
                            UserDefaults.standard.setLastnameP(value: apellidoP)
                            UserDefaults.standard.setLastnameM(value: apellidoM)
                            UserDefaults.standard.setNumber(value: numero)
                            UserDefaults.standard.setNameComplete(value: username)
                            UserDefaults.standard.setEmail(value: email!)
                            completion(nil, nil)
                            
                        }catch let error{
                            print(error.localizedDescription)
                            completion(nil, error)
                        }
                    }
                  
                    return
                }
        }
    }
    
    // MARK: - Services
    //func requestFetchPhoto(with id: Int, completion: @escaping (Photo?, Error?) -> ()) {
    //    let url = "\(photoUrl)/\(id)"
    //    let headers: HTTPHeaders = [
     //       "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
     //   ]
     //   print("URL \(url) \(headers)")
     //   Alamofire.request(url,headers: headers).responsePhoto { response in
     //       if let error = response.error {
     //           completion(nil, error)
     //           return
     //       }
     //       if let photo = response.result.value {
     //           completion(photo, nil)
     //           return
     //       }
     //   }
   // }
    
    //Operadores
    func requestFetchOperadores(completion: @escaping (Operador?, Error?) -> ()){
        let url = "\(endPoint)Operador/GetAllOperadores"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url,headers: headers)")
        Alamofire.request(url,headers: headers).responseOperador { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let operador = response.result.value {
                completion(operador, nil)
                return
            }
        }
    }
    
    //Operadores
    func requestFetchPreguntas(completion: @escaping (Pregunta?, Error?) -> ()){
        let url = "\(endPoint)Pregunta/GetAllPreguntas"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responsePregunta { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let pregunta = response.result.value {
                completion(pregunta, nil)
                return
            }
        }
    }
    
    func requestFetchReclamo(completion: @escaping (Reclamo?, Error?) -> ()){
        let url = "\(endPoint)Reclamo/GetAllTipoReclamo"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseReclamo { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let reclamo = response.result.value {
                completion(reclamo, nil)
                return
            }
        }
    }
    
    func requestFetchPlantilla(completion: @escaping (Plantilla?, Error?) -> ()){
        let url = "\(endPoint)Reclamo/GetAllPlantillaReclamo"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responsePlantilla { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let plantilla = response.result.value {
                completion(plantilla, nil)
                return
            }
        }
    }
    
    func requestFetchSendQuestion(with postPregunta: PostPregunta,completion: @escaping (AnyObject?, Error?) -> ()){
        let urlString = "\(endPoint)Usuario/CreateUsuarioPregunta"
        let decoder = try? JSONEncoder().encode(postPregunta.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let json = JSON(jsonData)
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        Alamofire.request(request).responseJSON{ response in

            if let statusCode = response.response?.statusCode{
                print("statusCode \(statusCode)")
                completion(nil, nil)
                return
            }
            if let error = response.error {
                print("Respuesta \(error)")
                completion(nil, error)
                return
            }
            if let _postPregunta = response.result.value {
                print("Usuario \(_postPregunta)")
                completion(nil, nil)
                return
            }
        }
    }
    
    //COBERTURA
    func requestFetchAllDepartamento(completion: @escaping (Departamento?, Error?) -> ()){
        let url = "\(cobertura)CoberturaMovil/GetAllDepartamentos"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        Alamofire.request(url,headers: headers).responseDepartamento{ response in
            
            if(response.response?.statusCode == 401){
                completion(nil, nil)
            }
            
            if let error = response.error {
                completion(nil, error)
                return
            }
            
            if let departamento = response.result.value {
                completion(departamento, nil)
                return
            }
        }
    }
    
    func requestFetchOficina(with id: String, with nombre: String ,completion: @escaping (Oficina?, Error?) -> ()) {
        guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        guard let Nombre = nombre.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _modeloCelular = modeloCelular.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(cobertura)CoberturaMovil/GetOficinasOsiptel?IdDepartamento=\(id)&NombreDepartamento=\(Nombre)&SistemaOperativo=\(sistemaOperativo)&MarcaCelular=\(marcaCelular)&ModeloCelular=\(_modeloCelular)&NumeroCelular=\(numeroCelular)&MAC=\(MAC)&NombreUsuario=\(NombreUsuario)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]

        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseOficina { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let oficina = response.result.value {
                completion(oficina, nil)
                return
            }
        }
    }
    
    func requestFetchMarca(completion: @escaping (Marca?, Error?) -> ()) {
        let url = "\(sigem)Problema/GetAllMarcas"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseMarca { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let marca = response.result.value {
                completion(marca, nil)
                return
            }
        }
    }
    
    func requestFetchModeloByMarca(with id: Int, completion: @escaping (Modelo?, Error?) -> ()) {
        let url = "\(sigem)Problema/GetModelosByMarca?idMarca=\(id)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseModelo { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let modelo = response.result.value {
                completion(modelo, nil)
                return
            }
        }
    }
    
    func requestFetchEmpresa(completion: @escaping (Empresa?, Error?) -> ()) {
        let url = "\(sigem)Problema/GetAllEmpresas"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseEmpresa { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let empresa = response.result.value {
                completion(empresa, nil)
                return
            }
        }
    }
    
    func requestFetchProblema(completion: @escaping (Problema?, Error?) -> ()) {
        let url = "\(sigem)Problema/GetAllProblemasDetectados"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseProblema { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let problema = response.result.value {
                completion(problema, nil)
                return
            }
        }
    }
    
    func requestFetchTipoProblema(completion: @escaping (TipoProblema?, Error?) -> ()) {
        let url = "\(cobertura)CoberturaMovil/GetTiposProblemas"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseTipoProblema { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let tipoProblema = response.result.value {
                completion(tipoProblema, nil)
                return
            }
        }
    }
    
    //Provincia
    func requestFetchProvincia(with id: String, completion: @escaping (Provincia?, Error?) -> ()) {
       
        guard let _Dep = id.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }

        
        let url = "\(cobertura)CoberturaMovil/GetProvinciasByDepartamento?codDepartamento=\(_Dep)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseProvincia { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let provincia = response.result.value {
                completion(provincia, nil)
                return
            }
        }
    }
    
    //Distrito
    func requestFetchDistrito(with id: String, with Prov: String, completion: @escaping (Distrito?, Error?) -> ()) {
        
        
        guard let _Dep = id.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _Prov = Prov.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(cobertura)CoberturaMovil/GetDistritosByDepartamentoAndProvincia?codDepartamento=\(_Dep)&codProvincia=\(_Prov)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseDistrito { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let distrito = response.result.value {
                completion(distrito, nil)
                return
            }
        }
    }
    
    
    //Localidad
    func requestFetchLocalidad(with Dep: String, with Prov: String,with Dist: String, completion: @escaping (Localidad?, Error?) -> ()) {
        
        
        guard let _Dep = Dep.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _Prov = Prov.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _Dist = Dist.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(cobertura)CoberturaMovil/GetLocalidadesByDepartamentoProvinciaAndDistrito?codDepartamento=\(_Dep)&codProvincia=\(_Prov)&codDistrito=\(_Dist)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseLocalidad { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let localidad = response.result.value {
                completion(localidad, nil)
                return
            }
        }
    }
    
    func requestFetchImei(with imei: String, with uuid: String, completion: @escaping (Imei?, Error?) -> ()) {
        
        guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _modeloCelular = modeloCelular.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(sigem)Sigem/GetDatosEquipoByImei?imei=\(imei)&ip=1&SistemaOperativo=\(sistemaOperativo)&MarcaCelular=\(marcaCelular)&ModeloCelular=\(_modeloCelular)&NumeroCelular=\(numeroCelular)&MAC=\(MAC)&NombreUsuario=\(NombreUsuario)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        guard let encodedURL = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseImei { response in
            
            print("Response \(response)")
            
            print("Result \(response.result.value)")

            print("Responses Bad Request \(response.response?.statusCode)")
            
            if response.response?.statusCode == 400{
                completion(nil,nil)
                return
            }
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let imei = response.result.value {
                completion(imei, nil)
                return
            }
        }
    }
    
    //COBERTURA
    func requestFetchGetCobertura(with departamento: String, with provincia: String, with distrito: String, with localidad: String, with operador: String, completion: @escaping (CoberturaMovil?, Error?) -> ()) {
        guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        guard let _departamento = departamento.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _provincia = provincia.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _distrito = distrito.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _localidad = localidad.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        guard let _modeloCelular = modeloCelular.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        
        
        let url = "\(cobertura)CoberturaMovil/GetCoberturaByOperadora?Departamento=\(_departamento)&Provincia=\(_provincia)&Distrito=\(_distrito)&Localidad=\(_localidad)&Operador=\(operador)&IP=\(IP)&SistemaOperativo=\(sistemaOperativo)&MarcaCelular=\(marcaCelular)&ModeloCelular=\(_modeloCelular)&NumeroCelular=\(numeroCelular)&MAC=\(MAC)&NombreUsuario=\(NombreUsuario)"
        print("URL \(url) \(headers)")
        
        guard let encodedURL = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        Alamofire.request(url,headers: headers).responseCoberturaMovil { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let imei = response.result.value {
                completion(imei, nil)
                return
            }
        }
    }
    
    //PostImei
    func requestFetchCreateProblemImei(with imeiPost: PostImei,completion: @escaping (ResponseUsuario?, Error?,String,String) -> ()){
        let urlString = "\(sigem)Sigem/CreateReportarProblemaImei"
        let decoder = try? JSONEncoder().encode(imeiPost.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let json = JSON(jsonData)
        print("URL \(urlString)")
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON{ response in
            //Response
        print("Respuesta \(json)")

            if let httpStatusCode = response.response?.statusCode {
                print("Response result")
                print(response.result.value)
                var mensaje = ""
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary {
                    print("dictionnary")
                    print(dictionnary)
                    print("httpStatusCode")
                    print(httpStatusCode)
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray {
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        mensaje = yourArray.joined(separator: ", ")
                    }
                }
                
                switch httpStatusCode{
                case 200:
                    completion(nil, nil, "200", mensaje)
                    return
                case 400:
                    completion(nil, nil, "400", mensaje)
                    return
                case 500:
                    completion(nil, response.error, "400", mensaje)                    
                    return
                default: break
                    
                }
                
            }
        }
    }
    
    func requestFetchCreateReporteCobertura(with coberturaPost: PostCobertura,completion: @escaping (ResponseCobertura?, Error?, String?,String?) -> ()){
        let urlString = "\(cobertura)CoberturaMovil/CreateReporteCobertura"
        let decoder = try? JSONEncoder().encode(coberturaPost.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let json = JSON(jsonData)
        print("URL \(urlString)")
        print("JSON \(json)")
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON{ response in
            //Response
            var mensaje = ""
            if let httpStatusCode = response.response?.statusCode {
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary {
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray {
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        mensaje = yourArray.joined(separator: ", ")
                    }else{
                        mensaje =  dictionnary.object(forKey: "numeroEncuesta") as! String
                    }
                }
                
                switch httpStatusCode{
                case 200:
                    completion(nil, nil, "200",mensaje)
                    return
                case 400:
                    completion(nil, nil, "400",mensaje)
                    return
                case 500:
                    completion(nil, nil, "500",mensaje)
                    return
                default: break
                    
                }
    
            }
        }
    }
    //DESCARGAR EXPEDIENTE
    func requestFetchDescargarExpediente(with idExpediente: String, with numeroDocumento: String, with idUsuario: String, with nombreCompleto: String , completion: @escaping (String?, Error?, String?, String?) -> ()) {
        guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        /*let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]*/
        let urlString = "\(trasu)Trasu/GetDescargarExpediente?IdExpediente=\(idExpediente)&NumeroDocumento=\(numeroDocumento)&IdUsuario=\(idUsuario)&NombreCompleto=\(NombreUsuario)"
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        
        //print("URL -> \(url) \(headers)")
        //Alamofire.request(url,headers: headers).responseExpedienteDescarga{
        Alamofire.request(request).responseJSON{
            response in
            if let httpStatusCode = response.response?.statusCode{
                var mensaje = ""
                var url = ""
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary{
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray{
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        mensaje = yourArray.joined(separator: ", ")
                    }else{
                        url =  dictionnary.object(forKey: "url") as! String
                    }
                }
                switch httpStatusCode{
                case 200:
                    completion(url, nil, "200",nil)
                    return
                case 400:
                    completion(nil, nil, "400",mensaje)
                    return
                case 500:
                    completion(nil, nil, "500",mensaje)
                    return
                default: break
                    
                }
           /* if let error = response.error{
                completion(nil, error, mensaje)
                return
            }
            if let expedienteDescarga = response.result.value {
                completion(nil, nil, nil)
                return
            }*/
        }
    }
}
    /*
     //Descarga de archivos
     func requestFetchDescargarExpediente(with idExpediente: String, with numeroDocumento: String, with idUsuario: String, with nombreCompleto: String , completion: @escaping (ExpedienteDescarga?, Error?, String?) -> ()) {
     
     guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
     return
     }
     
     let url = "\(trasu)Trasu/GetDescargarExpediente?IdExpediente=\(idExpediente)&NumeroDocumento=\(numeroDocumento)&IdUsuario=\(idUsuario)&NombreCompleto=\(NombreUsuario)"
     let headers: HTTPHeaders = [
     "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
     ]
     print("URL -> \(url) \(headers)")
     Alamofire.request(url,headers: headers).responseExpedienteDescarga { response in
     var mensaje = ""
     print("response \(response)")
     if let error = response.error {
     completion(nil, error, mensaje)
     return
     }
     if let expedienteDescarga = response.result.value {
     completion(expedienteDescarga, nil, nil)
     return
     }
     }
     }
     */
    
    
    func requestFetchValidarLogin(with correo: String, with clave: String, completion: @escaping (ResponseLogin?, String?) -> ()) {
        let url = "\(trasu)Trasu/ValidarLogin?Correo=\(correo)&Clave=\(clave)&Ip=1"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseResponseLogin { response in
            
            if let error = response.error {
                //completion(nil, error)
                print("respoonse \(response)")
                print(response.result.value)
                if let  login = response.result.value as? NSDictionary{
                    let messages =  login[0] as! String
                    print("Messages \(messages)")
                    completion(nil, messages)
                    return
                }
                
                completion(nil, "El correo electrónico y/o contraseña ingresados son incorrectos. Vuelva a intentar.")
                return
            }
            if let  login = response.result.value {
                completion(login, nil)
                return
            }
        }
    }
    
    
    func requestFetchCreatePostTrasu(with trasuPost: PostCuenta,completion: @escaping (String?, Error?, String?) -> ()){
        let urlString = "\(trasu)Trasu/CreateRegistroCuentaAcceso"
        let decoder = try? JSONEncoder().encode(trasuPost.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let json = JSON(jsonData)
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        Alamofire.request(request).responseJSON { response in
            if let error = response.error {
                completion(nil ,error, nil)
            }
            if response.response?.statusCode  == 400{
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary {
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray {
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        let mensaje = yourArray.joined(separator: ", ")
                         completion("error", nil, mensaje)
                    }
                }
            }else if response.response?.statusCode  == 200{
                print(response.result.value)
                completion("Ok", nil, nil)
            }
        }
    }
    
    func requestFetchCreateEventoLog(with trasuPost: BodyEntityLog,completion: @escaping (String?, Error?, String?) -> ()){
        let urlString = "\(endPoint)EventoLog/CreateEventoLog"
        let decoder = try? JSONEncoder().encode(trasuPost.self)
        let jsonString = String(data: decoder!, encoding: .utf8)
        let jsonData = jsonString!.data(using: .utf8, allowLossyConversion: true)!
        let json = JSON(jsonData)
        print("URL \(urlString)")
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        print("json \(json)")
        Alamofire.request(request).responseJSON { response in
            if let error = response.error {
                completion(nil ,error, nil)
            }
            if response.response?.statusCode  == 400{
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary {
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray {
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        let mensaje = yourArray.joined(separator: ", ")
                         completion("error", nil, mensaje)
                    }
                }
            }else if response.response?.statusCode  == 200{
                print(response.result.value)
                completion("Ok", nil, nil)
            }
        }
    }
    
    func requestFetchListarTipoDocumento(completion: @escaping (TipoDocumento?, Error?) -> ()) {
        let url = "\(trasu)Trasu/ListarTipoDocumento"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseTipoDocumento { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let tipoDocumento = response.result.value {
                completion(tipoDocumento, nil)
                return
            }
        }
    }
    
    func requestFetchListarEstados(completion: @escaping (Estados?, Error?) -> ()) {
        let url = "\(trasu)Trasu/ListarEstados"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseEstados { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let estados = response.result.value {
                completion(estados, nil)
                return
            }
        }
    }
    
    
    func requestFetchConsultarExpediente(with numeroDocumento: String, with estado: String, with fechaInicio: String, with fechaFin: String, completion: @escaping (Expediente?, Error?, String) -> ()) {
        
        var Estado = "&Estado=0"
        var FechaInicio = ""
        var FechaFin = ""
        
        if !estado.isEmpty{
            guard let _estado = estado.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                return
            }
            Estado = "&Estado=\(_estado)"
        }
        
        if !fechaInicio.isEmpty{
            FechaInicio = "&FechaInicio=\(fechaInicio)"
        }
        
        if !fechaFin.isEmpty{
            FechaFin = "&FechaFin=\(fechaFin)"
        }
        
        guard let NombreUsuario = nombreUsuario.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        guard let _modeloCelular = modeloCelular.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        let url = "\(trasu)Trasu/GetConsultarExpediente?NumeroDocumento=\(numeroDocumento)\(Estado)\(FechaInicio)\(FechaFin)&IP=\(IP)&SistemaOperativo=\(sistemaOperativo)&MarcaCelular=\(marcaCelular)&ModeloCelular=\(_modeloCelular)&NumeroCelular=\(numeroCelular)&MAC=\(MAC)&NombreUsuario=\(NombreUsuario)"
        
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        guard let encodedURL = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseExpediente{ response in
            
            if response.response?.statusCode == 200{
                if let expediente = response.result.value {
                    completion(expediente,nil, "ok")
                    return
                }
            }
            
            if response.response?.statusCode == 400{
                let responseMessage = response.result.value as? NSDictionary
                if let dictionnary : NSDictionary = responseMessage as? NSDictionary {
                    if let newArr : NSArray = dictionnary.object(forKey: "messages") as? NSArray {
                        let yourArray = NSMutableArray(array: newArr) as! [String]
                        let mensaje = yourArray.joined(separator: ", ")
                        completion(nil, nil, mensaje)
                        return
                    }
                }
                
                
            }
            if let error = response.error {
                completion(nil, error, "ok")
                return
            }
            if let expediente = response.result.value {
                completion(expediente, nil, "ok")
                return
            }
        }
    }
    
    
    //ListarServicios
    func requestFetchListarServicio(completion: @escaping (Servicio?, Error?) -> ()) {
        let url = "\(trasu)Trasu/ListarServicio"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseServicio { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let servicio = response.result.value {
                completion(servicio, nil)
                return
            }
        }
    }
    
    //Cobertura reporte
    func requestFetchListarReporteCobertura(with departamento: String, with provincia: String, with distrito: String, with localidad: String , completion: @escaping (ReporteCobertura?, Error?) -> ()) {
        
        guard let _localidad = localidad.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                return
        }
        guard let _distrito = distrito.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _provincia = provincia.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _departamento = departamento.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(cobertura)CoberturaMovil/GetListReporteCobertura?Departamento=\(_departamento)&Provincia=\(_provincia)&Distrito=\(_distrito)&Localidad=\(_localidad)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL -> \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseReporteCobertura { response in
            print("response \(response)")
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let reporteCobertura = response.result.value {
                completion(reporteCobertura, nil)
                return
            }
        }
    }

    
    
    
    
    //Lista de Servicios
    
    func requestFetchTiempoTrasu(completion: @escaping (TiempoTrasu?, Error?) -> ()) {
        let url = "\(trasu)Trasu/GetTiempoSessionExpediente"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseTiempoTrasu { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let tiempoTrasu = response.result.value {
                completion(tiempoTrasu, nil)
                return
            }
        }
    }
    
    func requestFetchServiciosOperador(with operador: String, completion: @escaping (ServicioOperador?, Error?) -> ()) {
        let url = "\(cobertura)CoberturaMovil/GetServiciosByOperador?IdEmpresaOperadora=\(operador)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseServicioOperador { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let servicioOperador = response.result.value {
                completion(servicioOperador, nil)
                return
            }
        }
    }

    
    func requestFetchServiciosOperadorEqual(with operador: String, completion: @escaping (ServicioOperador?, Error?) -> ()) {
        let url = "\(cobertura)CoberturaMovil/GetServiciosByOperadorEqual?IdEmpresaOperadora=\(operador)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseServicioOperador { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let servicioOperador = response.result.value {
                completion(servicioOperador, nil)
                return
            }
        }
    }
    
    func requestFetchServiciosApplicationGlobal(with global: String, completion: @escaping (AppGlobal?, Error?) -> ()) {
        let url = "\(endPoint)Operador/GetApplicationGlobal?Aplicacion=APPMVL&Global=\(global)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseJSON { response in
            
            print("Response \(response)")
            
            if response.result.value != nil{
                if response.response?.statusCode == 200{
                    let JsonData = JSON(response.result.value!)
                    let nombre = "\(JsonData["nombre"])"
                    let valor = "\(JsonData["valor"])"
                    
                    switch nombre {
                    case "ENLACE_PUNKU":
                        UserDefaults.standard.setAppGlobalPunku(value: valor)
                    case "ENLACE_COMPARATEL":
                        UserDefaults.standard.setAppGlobalCompTel(value: valor)
                    case "ENLACE_COMPARAMOVIL":
                        UserDefaults.standard.setAppGlobalCompMov(value: valor)
                    case "ENLACE_COBERTURA":
                        UserDefaults.standard.setAppGlobalEnlaceCob(value: valor)
                    default:
                        break
                    }
                }
            }
            
            if response.error != nil {
                let error = response.error
                //completion(error)
                return
            }


        }
//        print("URL \(url) \(headers)")
//        Alamofire.request(url,headers: headers).responseAppGlobal { response in
//            if let error = response.error {
//                completion(nil, error)
//                return
//            }
//            if let responseAppGlobal = response.result.value {
//                completion(responseAppGlobal, nil)
//                return
//            }
//        }
    }
    
    
    func requestFetchGetUbicacionActual(with departamento: String, with provincia: String, with distrito: String, with localidad: String , completion: @escaping (Ubicacion?, Error?) -> ()) {
        
        guard let _localidad = localidad.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _distrito = distrito.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _provincia = provincia.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        guard let _departamento = departamento.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            return
        }
        
        let url = "\(cobertura)CoberturaMovil/GetUbicacionActual?Departamento=\(_departamento)&Provincia=\(_provincia)&Distrito=\(_distrito)&Localidad=\(_localidad)"
        let headers: HTTPHeaders = [
            "Authorization": "\(UserDefaults.standard.getTypeToken()) \(UserDefaults.standard.getToken())"
        ]
        print("URL -> \(url) \(headers)")
        Alamofire.request(url,headers: headers).responseUbicacion { response in
            print("response \(response)")
            if let error = response.error {
                completion(nil, error)
                return
            }
            if let reporteCobertura = response.result.value {
                completion(reporteCobertura, nil)
                return
            }
        }
    }
    
    
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}
