//
//  AlertViewController.swift
//  Osiptel_app
//
//  Created by GYS on 3/22/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var titleview: UIView!
    
    @IBOutlet weak var icImage: UIImageView!
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var icValidate: UILabel!
    var visual:UIVisualEffect!
    
    var titulo = ""
    var mensaje = ""
    var closeFlag = false
    var imageValidate = false
    var closeAutomatic = false
    var sendVC = ""
    
    @IBAction func btnCloseAction(_ sender: Any) {
        dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visual = visualEffect.effect
        visualEffect.effect = nil
        btnClose.alpha = 1
        titleview.roundedCorners(top: true)
        lblTitle.text = titulo
        lblMessage.text = mensaje
        
        if imageValidate{
            icImage.image = UIImage.init(named: "ic_check")
        }else{
            icImage.image = UIImage.init(named: "ic_error")
        }
        
        if !closeFlag{
            btnClose.alpha = 0
        }else{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
            //let tapGesture = UITapGestureRecognizer(target: self, action: Selector("dismissModal"))
            tapGesture.cancelsTouchesInView = true
            visualEffect.addGestureRecognizer(tapGesture)
        }
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        viewAlert.center = visualEffect.center
        viewAlert.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewAlert.alpha = 0
        
        UIView.animate(withDuration: 0.3) {
            self.visualEffect.effect = self.visual
            self.visualEffect.alpha = 1
            self.viewAlert.alpha = 1
            self.viewAlert.transform = CGAffineTransform.identity
        }
        
        if closeAutomatic{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                if self.sendVC == "CustomTabBarView"{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView")
                    controller.navigationItem.setHidesBackButton(true, animated: false)
                    self.present(controller, animated: true, completion: nil)
                }else if self.sendVC == "Imei"{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
                    controller.selectedIndex = 1
                    self.present(controller, animated: true, completion: nil)
                }else{
                    self.dismiss()
                }
            }
        }
    }
    @objc func dismissModal(_ sender: UITapGestureRecognizer){
        dismiss()
    }
    
    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.visualEffect.effect = nil
            self.viewAlert.alpha = 0
            self.visualEffect.alpha = 0
            
        }) { (success:Bool) in
            if !self.imageValidate{
                self.dismiss(animated: true, completion: nil)
            }else if self.sendVC == "Imei"{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
                controller.selectedIndex = 1
                self.present(controller, animated: true, completion: nil)
            }else if self.sendVC == "Señal"{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
                controller.indexPosition = 3
                self.present(controller, animated: false, completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView")
                controller.navigationItem.setHidesBackButton(true, animated: false)
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
     
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
