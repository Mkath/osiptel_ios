//
//  DownloadFileTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/22/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol DownloadFileDelegate: class {
    func DescargarPdf(descarga : String, nombre: String)
    func DescargarWord(descarga : String, nombre: String)
}


class DownloadFileTableViewCell: UITableViewCell {
    var PlantillaItem : PlantillaElement!
    var delegate :DownloadFileDelegate?
    var archivoDescargaPdf = ""
    var archivoDescargaWord = ""
    var nombre = ""
    @IBOutlet weak var lblDownload: UILabel!
    @IBOutlet weak var btnDownloadPdf: UIButton!
    @IBOutlet weak var btnDownloadWord: UIButton!
    @IBAction func btnDownloadPDFAction(_ sender: Any) {
        delegate?.DescargarPdf(descarga: archivoDescargaPdf, nombre: nombre)
    }
    
    @IBAction func btnDownloadWORDAction(_ sender: Any) {
        delegate?.DescargarWord(descarga: archivoDescargaWord, nombre: nombre)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDownlaod(pregunta: PlantillaElement){
        PlantillaItem = pregunta
        archivoDescargaPdf = PlantillaItem.descripcionPDF ?? ""
        archivoDescargaWord = PlantillaItem.descripcionWORD ?? ""
        lblDownload.text = PlantillaItem.nombreArchivo
        nombre = PlantillaItem.nombreArchivo ?? ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
