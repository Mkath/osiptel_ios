//
//  CardViewController.swift
//  CardViewAnimation
//
//  Created by Brian Advent on 26.10.18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CardViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var handleArea: UIView!
    @IBOutlet weak var lblDepartamento: UILabel!
    @IBOutlet weak var lblProvincia: UILabel!
    @IBOutlet weak var lblDistrito: UILabel!
    @IBOutlet weak var lblLocalidad: UILabel!
    @IBOutlet weak var lblLatitud: UILabel!
    @IBOutlet weak var lblLongitud: UILabel!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnExpand: UIImageView!
    
    @IBOutlet weak var tblOperadoresSeñal: UITableView!
    
    
    var _reporteCobertura = ReporteCobertura()
    var operadoresSend = [[String:String]]()
    var operadoresFiltro : [String] = []
    var operadoresImagen = [String]()
    var tecnologiaArray = [String]()
    //ViewController Ver Reportes - Consultar
    var tieneCobertura = false
    var count = 0
    
    var CadenaOperador = ""
    var _coberturaMovil = CoberturaMovil()
    var stateView = false
    var _coberturaElement : CoberturaMovilElement!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count = 0
        tblOperadoresSeñal.delegate = self
        tblOperadoresSeñal.dataSource = self

        let nib = UINib.init(nibName: "operatorSignalCell", bundle: nil)
        tblOperadoresSeñal.register(nib, forCellReuseIdentifier: "operatorSignalCell")
        
        lblDepartamento.text = _coberturaElement.departamento
        lblProvincia.text = _coberturaElement.provincia
        lblDistrito.text = _coberturaElement.distrito
        lblLocalidad.text = _coberturaElement.localidad
        lblLatitud.text = _coberturaElement.latitud
        lblLongitud.text = _coberturaElement.longitud
        
        
        
        tblOperadoresSeñal.reloadData()
        
    }
    

    
    func attempFetchReporteCobertura(idDepartmento: String, idProvincia: String, idDistrito: String, localidad: String){
        DataService.shared.requestFetchListarReporteCobertura(with: idDepartmento, with: idProvincia, with: idDistrito, with: localidad ){ [weak self] (ReporteCobertura, error) in
            
            self!.stopAnimating()
            if let error = error {
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let ReporteCobertura = ReporteCobertura else{
                return
            }
            self?.updateReporteCobertura(with: ReporteCobertura)
        }
    }
    
    func updateReporteCobertura(with ReporteCobertura: ReporteCobertura) {
        _reporteCobertura = ReporteCobertura
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportAllTableViewController") as! ReportAllTableViewController
        let backItem = UIBarButtonItem()
        backItem.title = " "
        controller._reporte = _reporteCobertura
        navigationItem.backBarButtonItem = backItem
        
        let navigationController = UINavigationController(rootViewController: controller)
        //self.presentViewController(navigationController, animated: true, completion: nil)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnReportAction(_ sender: Any) {
        let size = CGSize(width: 308, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        attempFetchReporteCobertura(idDepartmento: lblDepartamento.text!, idProvincia: lblProvincia.text!, idDistrito: lblDistrito.text!, localidad: lblLocalidad.text!)
        
    }
    
    @IBAction func btnReportarAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReportCoverageViewController") as! ReportCoverageViewController
        let backItem = UIBarButtonItem()
        
        vc.title = "Reportar"
        
        vc.DepartamentoSegue = _coberturaElement.departamento
        vc.ProvinciaSegue = _coberturaElement.provincia
        vc.DistritoSegue = _coberturaElement.distrito
        vc.localidadSegue = _coberturaElement.localidad
        vc.LatitudSegue = lblLatitud.text!
        vc.LongitudSegue = lblLongitud.text!
        vc._longitudConsultada = Float(lblLongitud.text!)!
        vc._latitudConsultada = Float(lblLatitud.text!)!
        
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _coberturaElement != nil {
            return _coberturaElement.tecnologisOperador!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "operatorSignalCell", for: indexPath) as! operatorSignalTableViewCell
                    
        let original =  operadoresImagen[indexPath.row]
        let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        cell.imageOperator.sd_setImage(with: URL(string: encoded ?? "" ))
        _ = Int(_coberturaElement.nroReporte) ?? 0
        self.btnReport.setTitle("VER REPORTES", for: .normal)
        cell.viewTechnology.subviews.forEach { $0.removeFromSuperview() }
        cell.viewTechnology.subviews
        
        
        tecnologiaArray = _coberturaElement.tecnologisOperador![indexPath.row].tecnologiaArray
        var height: CGFloat = 0
        for tec in tecnologiaArray{
            if tec == "Sin Cobertura"{
                let rect = CGRect(origin: CGPoint(x:height, y:0), size: CGSize(width:200, height:35))
                var label = UILabel(frame: rect)
                cell.viewTechnology.backgroundColor = .white
                label.textColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                label.font = label.font.withSize(11)
                label.backgroundColor = UIColor.white
                label.layer.masksToBounds = true
                label.text = tec
                label.textAlignment = .center
                cell.viewTechnology.addSubview(label)
            }else {
                if tec != ""{
                    let rect = CGRect(origin: CGPoint(x:height, y:0), size: CGSize(width:35, height:35))
                    height += 41
                    cell.viewTechnology.backgroundColor = .white
                    var label = UILabel(frame: rect)
                    label.backgroundColor = UIColor(red:0.95, green:0.97, blue:1.00, alpha:1.0)
                    label.textColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                    label.layer.cornerRadius = label.frame.width/2
                    label.font = label.font.withSize(11)
                    label.layer.masksToBounds = true
                    label.text = tec
                    label.textAlignment = .center
                    cell.viewTechnology.addSubview(label)
                }
                
            }
        }
        return cell
    }
}

