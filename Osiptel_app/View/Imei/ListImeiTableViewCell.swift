//
//  ListImeiTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/1/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion

class ListImeiTableViewCell: AccordionTableViewCell {

    static let reuseIdentifier = "ImeiTableviewCell"
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var arrow: UIImageView! {
        didSet {
            arrow.tintColor = UIColor.white
        }
    }
    
    @IBOutlet weak var detailView: UIView!
    
    
    @IBOutlet weak var lblEstadoEquipo: UILabel!
    @IBOutlet weak var lblOperadora: UILabel!
    @IBOutlet weak var lblFecha: UILabel!

    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if expanded{
            detailView.roundedCorners(top: false)
            headerView.roundedCorners(top: true)
            headerView.backgroundColor = UIColor(red:0.93, green:0.54, blue:0.17, alpha:1.0)
        }else{

            //headerView.backgroundColor = UIColor.orange
            headerView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.86, alpha:1.0)
            headerView.roundedCornersAll()
            
        }
        if animated {
            let alwaysOptions: UIView.AnimationOptions = [.allowUserInteraction,
                                                          .beginFromCurrentState,
                                                          .transitionCrossDissolve]
            let expandedOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .curveEaseOut]
            let collapsedOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .curveEaseIn]
            let options = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
            UIView.transition(with: detailView, duration: 0.3, options: options, animations: {
                self.toggleCell()
            }, completion: nil)
        } else {
            toggleCell()
        }
    }
    
    // MARK: Helpers
    
    private func toggleCell() {
        detailView.isHidden = !expanded
        arrow.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
    

}
