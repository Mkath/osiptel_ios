//
//  RatingStartTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Cosmos

protocol RatingStartTableViewCellDelegate {
    func RatingStartViewCellSend(idPregunta: Int, respuesta: String)
}

class RatingStartTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCalificanos: UILabel!
    @IBOutlet weak var btnEnviarEncuesta: UIButton!
    @IBOutlet weak var starBtn: CosmosView!
    var PreguntaItem : PreguntaElement!
    var delegate: RatingStartTableViewCellDelegate?
    var rating = "5"
    
    @IBAction func SendQuestionAction(_ sender: Any) {

        rating = String(format: "%.0f",starBtn.rating)
        delegate?.RatingStartViewCellSend(idPregunta: PreguntaItem.idPregunta, respuesta: rating)
    }
    
    func setRate(pregunta: PreguntaElement){
        PreguntaItem = pregunta
        lblCalificanos.text = PreguntaItem.descripcion
        
    }
    
    override public func prepareForReuse() {
        starBtn.prepareForInterfaceBuilder()
        starBtn.settings.updateOnTouch = true
        starBtn.settings.fillMode = .full
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        starBtn.settings.fillMode = .full
        starBtn.settings.updateOnTouch = true
        
        starBtn.didTouchCosmos = { rating in
            print("Did Rating \(rating)")
        }
        
        starBtn.didFinishTouchingCosmos = {
            rating in
            //self.delegate?.ratingDidChange(rating)
            print("Rating \(rating)")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
