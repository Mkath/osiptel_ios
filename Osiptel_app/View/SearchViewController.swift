//
//  SearchViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol searchingDelegate : class {
    func searchAction(estado: String , desde: String, hasta: String)
}


class SearchViewController: UIViewController {

    
    @IBOutlet weak var txtDesde: UITextField!
    @IBOutlet weak var txtHasta: UITextField!
    @IBOutlet weak var txtEstado: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    weak var delegate: searchingDelegate?
    var _login : ResponseLogin!
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var titleview: UIView!
    var visual:UIVisualEffect!    
    var _tipoDocumento = TipoDocumento()
    var _estados = Estados()
    let myColor = UIColor.red
    private var PickerFechaDesde : UIDatePicker?
    private var PickerFechaHasta : UIDatePicker?
    public var PickerTipo : UIPickerView?
    public var PickerEstado : UIPickerView?
    var isValidate = false
    override func viewDidLoad() {
        super.viewDidLoad()

        visual = visualEffect.effect
        visualEffect.effect = nil
        btnClose.alpha = 1
        titleview.roundedCorners(top: true)
        var components = DateComponents()
        PickerFechaDesde = UIDatePicker()
        PickerFechaDesde?.datePickerMode = .date
        PickerFechaDesde?.maximumDate = Date()
        PickerFechaDesde?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        
        PickerFechaDesde?.tag = 0
        txtDesde?.inputView = PickerFechaDesde
        
        PickerFechaHasta = UIDatePicker()
        PickerFechaHasta?.datePickerMode = .date
        PickerFechaHasta?.maximumDate = Date()
        PickerFechaHasta?.addTarget(self, action: #selector(dateChanged2(datePicker:)), for: .valueChanged)
        
        PickerFechaHasta?.tag = 1
        txtHasta?.inputView = PickerFechaHasta
        PickerTipo = UIPickerView()

        PickerEstado = UIPickerView()
        
        PickerEstado?.tag = 3
        PickerEstado?.delegate = self
        PickerEstado?.dataSource = self
        txtEstado?.inputView = PickerEstado
        txtHasta.isEnabled = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(gestureRecognizer:)))
        tapGesture.cancelsTouchesInView = true
        visualEffect.addGestureRecognizer(tapGesture)
        
        let keyboard = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(keyboard)

        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        viewAlert.center = visualEffect.center
        viewAlert.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewAlert.alpha = 0
        
        UIView.animate(withDuration: 0.3) {
            self.visualEffect.effect = self.visual
            self.visualEffect.alpha = 1
            self.viewAlert.alpha = 1
            self.viewAlert.transform = CGAffineTransform.identity
            
        }
        
        attempFetchTipoDocumento()
        attempFetchEstados()
        //txtEstado.text = "Estado"
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    func attempFetchTipoDocumento(){
        DataService.shared.requestFetchListarTipoDocumento{ [weak self] (tipoDocumento, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let tipoDocumento = tipoDocumento else{
                return
            }
            self?.updateTipoDocumento(with: tipoDocumento)
        }
    }
    
    func updateTipoDocumento(with tipoDocumento: TipoDocumento) {
        _tipoDocumento = tipoDocumento
        _tipoDocumento.insert(TipoDocumentoElement.init(idcodigo: "0", descripcion: "Seleccionar"), at: 0)
        PickerTipo?.reloadAllComponents()
    }
    
    func attempFetchEstados(){
        DataService.shared.requestFetchListarEstados{ [weak self] (estados, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let estados = estados else{
                return
            }
            self?.updateEstados(with: estados)
        }
    }
    
    func updateEstados(with estados: Estados) {
        _estados = estados
        _estados.insert(Estado.init(idcodigo: "0", descripcion: "Seleccionar"), at: 0)
        PickerEstado?.reloadAllComponents()        
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        txtDesde.layer.borderWidth = 0
        txtDesde.text = dateFormatter.string(from: datePicker.date)
        txtHasta.isEnabled = true
        view.endEditing(true)
        
    }
    
    @objc func dateChanged2(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        txtHasta.layer.borderWidth = 0
        txtHasta.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    @objc func dismissModal(gestureRecognizer: UITapGestureRecognizer){
        dismiss()
    }
    
    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.visualEffect.effect = nil
            self.viewAlert.alpha = 0
            self.visualEffect.alpha = 0
            
        }) { (success:Bool) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBuscarAction(_ sender: Any) {
        
        isValidate = true

        if txtDesde.text != "" && txtHasta.text == ""{
            AppDelegate.showAlertView(vc: self, titleString: "", messageString: "Por favor ingrese ambas fechas")
            isValidate = false
        }
        
        if txtDesde.text != "" && txtHasta.text != "" {
        
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            let firstDate = formatter.date(from: txtDesde.text!)
            let secondDate = formatter.date(from: txtHasta.text!)
            
            if firstDate?.compare(secondDate!) == .orderedDescending {
                AppDelegate.showAlertView(vc: self, titleString: "", messageString: "Por favor ingrese una  fecha valida")
                isValidate = false
            }
        }
        
        

        
        if isValidate{
            
            if txtEstado.text == "Seleccione"{
                txtEstado.text = ""
            }
            
            if txtHasta.text == ""{
                txtDesde.text = ""
            }
            
            delegate?.searchAction( estado: txtEstado.text!, desde: txtDesde.text! , hasta: txtHasta.text!)
            dismiss()
        }
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        dismiss()
    }

}
extension SearchViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      
        if  pickerView.tag == 2{
            return _tipoDocumento.count
        }
        
        if  pickerView.tag  == 3{
            return _estados.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if  pickerView.tag == 3{
            if _estados.count > 0{
                return _estados[row].descripcion
            }
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {


        
        if pickerView.tag == 3{
            if _estados.count > 0{
                txtEstado.layer.borderWidth = 0
                txtEstado.text = _estados[row].descripcion
            }
        }
    }

    
}
extension SearchViewController: UITextFieldDelegate{
    
}
