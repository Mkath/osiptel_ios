//
//  InformationOperatorTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class InformationOperatorTableViewCell: UITableViewCell {

    @IBOutlet weak var LblDepartamento: UILabel!
    @IBOutlet weak var LblProvincia: UILabel!
    @IBOutlet weak var LblDistrito: UILabel!
    @IBOutlet weak var LblLocalidad: UILabel!
    @IBOutlet weak var LblLatitud: UILabel!
    @IBOutlet weak var LblLongitud: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
