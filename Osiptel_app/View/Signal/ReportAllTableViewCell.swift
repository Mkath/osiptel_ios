//
//  ReportAllTableViewCell.swift
//  Osiptel_app
//
//  Created by GYS on 3/19/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion

protocol reportAllDelegate : class{
    func viewComentary(reporte : String)
}


class ReportAllTableViewCell: AccordionTableViewCell {

    static let reuseIdentifier = "ReportAllTableviewCell"
    
    @IBOutlet weak var lblUsuario: UILabel!
    @IBOutlet weak var lblEmpresa: UILabel!
    @IBOutlet weak var lblFechavisita: UILabel!
    @IBOutlet weak var lblFechaRegistro: UILabel!
    var ReporteItem: ReporteCoberturaElement!
    var comentario = ""
    weak var delegate : reportAllDelegate?
    @IBAction func VerComentarioAction(_ sender: Any) {
        delegate?.viewComentary(reporte: comentario)
    }
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var arrow: UIImageView! {
        didSet {
            arrow.tintColor = UIColor.white
        }
    }
    
    @IBOutlet weak var btnComentario: UIButton!
    @IBOutlet weak var detailView: UIView!
    
    func setReporte(reporte: ReporteCoberturaElement){
        lblEmpresa.text = reporte.empresa!
        lblUsuario.text = reporte.nombre!
        lblFechavisita.text = reporte.fechaVisita!
        lblFechaRegistro.text = reporte.fechaRegistro!        
        print("comentario \(reporte)")
        if reporte.comentario == nil {
            btnComentario.alpha = 0
        }else{
            comentario = reporte.comentario!
            btnComentario.alpha = 1
        }
    }

    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if expanded{
            detailView.roundedCorners(top: false)
            headerView.roundedCorners(top: true)
            headerView.backgroundColor = UIColor(red:0.93, green:0.54, blue:0.17, alpha:1.0)
        }else{
            headerView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.86, alpha:1.0)
            headerView.roundedCornersAll()
            
        }
        if animated {
            let alwaysOptions: UIView.AnimationOptions = [.allowUserInteraction,
                                                          .beginFromCurrentState,
                                                          .transitionCrossDissolve]
            let expandedOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .curveEaseOut]
            let collapsedOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .curveEaseIn]
            let options = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
            UIView.transition(with: detailView, duration: 0.3, options: options, animations: {
                self.toggleCell()
            }, completion: nil)
        } else {
            toggleCell()
        }
    }
    
    private func toggleCell() {
        detailView.isHidden = !expanded
        print("Expanded \(expanded)")
        arrow.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
    
}
