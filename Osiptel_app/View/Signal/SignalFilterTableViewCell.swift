//
//  SignalFilterTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol SignalFilterTableViewCellDelegate : class {
    func SignalFilterTableViewCellDidSwitch(Operador: String, Imagen: String, Estado: Bool)
}

class SignalFilterTableViewCell: UITableViewCell {

    @IBAction func SwitchControl(_ sender: Any) {
    }
    @IBOutlet weak var imageOperator: UIImageView!
    
    @IBOutlet weak var filterSwitch: UISwitch!
    weak var delegate: SignalFilterTableViewCellDelegate?
    var idOperador : Int!
    var filterSwitchIsOn = false
    var OperadorItem : OperadorElement!
    var Operador : String = ""
    var Imagen : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        filterSwitch.addTarget(self, action: #selector(SignalSwitchValueChanged(_:)), for: .valueChanged)

    }
    
    func setOperador(operador: OperadorElement){
        OperadorItem = operador
        
        let original =  OperadorItem.nombreImagen
        let encoded = original!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        imageOperator.sd_setImage(with: URL(string: encoded ?? ""))
    }

    @objc func SignalSwitchValueChanged(_ sender : UISwitch!){

        Operador = "\(OperadorItem.codigoOperador)"
        Imagen = OperadorItem.nombreImagen as! String

        if sender.isOn {
            filterSwitchIsOn =  true
            delegate?.SignalFilterTableViewCellDidSwitch(Operador: Operador ,Imagen: Imagen, Estado: filterSwitchIsOn)
        
        } else {
            filterSwitchIsOn =  false
            delegate?.SignalFilterTableViewCellDidSwitch(Operador: Operador ,Imagen: Imagen, Estado: filterSwitchIsOn)
        }        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

}
