//
//  LaunchScreenView.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Lottie

class LaunchScreenView: UIViewController {
    @IBOutlet weak var AnimationView: UIView!
    
    var encuestaModel = Encuesta()
    var usuarioModel = Usuario()
    var isFirst = true
    
    var Opciones = [["id":"1","nombre":"Consulta IMEI","imagen":"ic_consulta_IMEI"],["id":"2","nombre":"Señal OSIPTEL","imagen":"ic_senal_osiptel"],["id":"4","nombre":"Expedientes Trasu","imagen":"ic_expedientes"],["id":"5","nombre":"Guía de Información","imagen":"ic_guia_informativa"],["id":"6","nombre":"Verifica tu línea","imagen":"ic_verifica_linea"],["id":"7","nombre":"Nuestras oficinas","imagen":"ic_nuestras"],["id":"8","nombre":"Comparatel","imagen":"ic_comptel"],["id":"9","nombre":"Comparamóvil","imagen":"ic_comparamovil"],["id":"10","nombre":"Punku","imagen":"ic_punku"]] as [[String:AnyObject]]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let animationView = LOTAnimationView(name: "loading_animated")
        self.AnimationView.addSubview(animationView)
        animationView.loopAnimation = true
        animationView.contentMode = .scaleAspectFit
        animationView.center = self.view.center
        animationView.frame = CGRect(x: 50, y: -40, width: 180, height: 180)
        animationView.play()
        
       /* if (Connectivity.isConnectedToInternet()) {
            
            DataService.shared.requestToken()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                //UUID
                UserDefaults.standard.setOpciones(value: self.Opciones)
                
                let UUID = UserDefaults.standard.getUUID()
                let UserId = UserDefaults.standard.getUserID() as! String ?? ""
                var isUUIDEmpty = true
                
                while isUUIDEmpty{
                    isUUIDEmpty = UUID.isEmpty
                    if !UserDefaults.standard.isLoggedIn(){
                        self.attempFetchUsuario(withId: UUID)
                    }else{
                        
                        if !UserDefaults.standard.isCalificanos(){
                            //Envio de UserID -> Para saber si dispone de encuesta
                            self.attempReFetchUsuario(withId: UUID)
                            self.attempFetchEncuesta(withId: UserId)
                        }else{
                            self.attempFetchEncuesta(withId: UserId)
                            //self.rootviewcontroller()
                        }
                    }
                }
            }
            
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotConnectionViewController") as! NotConnectionViewController
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }*/
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            //UUID
            UserDefaults.standard.setOpciones(value: self.Opciones)
            
            let UUID = UserDefaults.standard.getUUID()
            let UserId = UserDefaults.standard.getUserID() as! String ?? ""
            var isUUIDEmpty = true
            
            while isUUIDEmpty{
                isUUIDEmpty = UUID.isEmpty
                if !UserDefaults.standard.isLoggedIn(){
                    self.attempFetchUsuario(withId: UUID)
                }else{
                    
                    if !UserDefaults.standard.isCalificanos(){
                        //Envio de UserID -> Para saber si dispone de encuesta
                        self.attempReFetchUsuario(withId: UUID)
                        self.attempFetchEncuesta(withId: UserId)
                    }else{
                        self.attempFetchEncuesta(withId: UserId)
                        //self.rootviewcontroller()
                    }
                }
            }
        
        }
            /* DataService.shared.requestToken(){ [weak self] (error) in
            if let error = error {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "NotConnectionViewController") as! NotConnectionViewController
                vc.modalPresentationStyle = .overFullScreen
                self!.present(vc, animated: false, completion: nil)
            }
        }*/
        
      //
        
        DataService.shared.refreshRequestToken(){[weak self] (String, error) in
            if (String == "Success"){
                self!.attempFetchServiciosApplicationGlobal(global: "ENLACE_PUNKU")
                self!.attempFetchServiciosApplicationGlobal(global: "ENLACE_COMPARATEL")
                self!.attempFetchServiciosApplicationGlobal(global: "ENLACE_COMPARAMOVIL")
                self!.attempFetchServiciosApplicationGlobal(global: "ENLACE_COBERTURA")
            }
        }
    }
    
    func attempFetchServiciosApplicationGlobal(global: String){
        DataService.shared.requestFetchServiciosApplicationGlobal(with: global){ [weak self] (servicio, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let servicio = servicio else{
                return
            }
            print("\(servicio)")
        }
    }


    func rootviewcontroller(){
        
        let IsLogged = UserDefaults.standard.isLoggedIn()
        
        print("Logged \(IsLogged)")
        
        if IsLogged{
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                
                let sistemaOperativo = "iOS"
                let marcaCelular = "Apple"
                let modeloCelular = UIDevice.current.modelName as! String
                let accion = global.ACTION_GETIN_APP
                let MAC = UserDefaults.standard.getUUID()
                var nombreUsuario = ""
                var numeroCelular = ""
                if(UserDefaults.standard.getNameComplete() != nil){
                    nombreUsuario = UserDefaults.standard.getUserID()
                    numeroCelular = UserDefaults.standard.getNumber()
                }
                
                let PostB = BodyEntityLog.init(sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, accion: accion, mac: MAC, nombreUsuario: nombreUsuario)
                self.attempFetchCreateEventoLog(withId: PostB)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView")
                controller.modalPresentationStyle = .overFullScreen
                self.present(controller, animated: true, completion: nil)
            }
        }else{

            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "SelectedViewController")
                controller.modalPresentationStyle = .overFullScreen
                self.present(controller, animated: true, completion: nil) 
            }
        }
    }
    
    func attempFetchUsuario(withId mac: String){
        DataService.shared.requestFetchUserData(with: mac) {[weak self] (usuario, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                self!.attempFetchUsuario(withId: mac)
                return
            }
            guard let usuario = usuario else{
                return
            }
            self?.updateUsuario(with: usuario)
        }
    }
    
    func attempReFetchUsuario(withId mac: String){
        DataService.shared.requestFetchUserData(with: mac) {[weak self] (usuario, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                return
            }
            guard let usuario = usuario else{
                return
            }
            if usuario.count == 0{
                UserDefaults.standard.setLoggedIn(value: false)
                UserDefaults.standard.setUserID(value: "")
                UserDefaults.standard.setName(value: "")
                UserDefaults.standard.setLastnameP(value: "")
                UserDefaults.standard.setLastnameM(value: "")
                UserDefaults.standard.setEmail(value: "")
                UserDefaults.standard.setNameComplete(value: "")
                UserDefaults.standard.setNumber(value:"")
                UserDefaults.standard.setCalificanos(value: false)
                UserDefaults.standard.setLoginTrasu(value: "")
                UserDefaults.standard.setRegisterTrasu(value: "")
                self!.rootviewcontroller()
            }else{
                self!.rootviewcontroller()
            }
        }
    }
    
    func updateUsuario(with usuario: Usuario) {
        
        usuarioModel = usuario
        print("Usuario modelo \(usuario)")
        print("usuarioModel modelo \(usuarioModel.count)")
        if usuarioModel.count > 0{
            UserDefaults.standard.setLoggedIn(value: true)
            UserDefaults.standard.setUserID(value: "\(usuarioModel.last!.idUsuario)")
            UserDefaults.standard.setName(value: usuarioModel.last!.nombre)
            UserDefaults.standard.setLastnameP(value: usuarioModel.last!.apellidoPaterno)
            UserDefaults.standard.setLastnameM(value: usuarioModel.last!.apellidoMaterno
            )
            UserDefaults.standard.setEmail(value: usuarioModel.last!.correoElectronico as! String)
            UserDefaults.standard.setNameComplete(value: "\(usuarioModel.last!.nombre) \(usuarioModel.last!.apellidoPaterno) \(usuarioModel.last!.apellidoMaterno)")
            UserDefaults.standard.setNumber(value: usuarioModel.last?.nroTelefono as! String)
            print("\(UserDefaults.standard.isLoggedIn())")
            attempFetchEncuesta(withId: UserDefaults.standard.getUserID())
        }else{
            rootviewcontroller()
        }
    }
    
    func attempFetchEncuesta(withId user: String){
        DataService.shared.requestFetchCalificanos(with: user) { [weak self] (encuesta, error) in
            if ( encuesta == nil && error == nil) {
                 DataService.shared.refreshRequestToken(){[weak self] (String, error) in
                    if (String == "Success"){
                        if(self!.isFirst){
                            self!.attempFetchEncuesta(withId: UserDefaults.standard.getUserID())
                            self!.isFirst = false
                        }
                    }
                }
                /*AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)*/
            }
            guard let encuesta = encuesta else{
                return
            }
            
            self?.updateEncuesta(with: encuesta)
        }
    }
    
    func updateEncuesta(with encuesta: Encuesta) {
        
        encuestaModel = encuesta
        if encuestaModel.count > 0{

            if encuestaModel.first?.idUsuario != nil{
                
                Opciones = [["id":"1","nombre":"Consulta IMEI","imagen":"ic_consulta_IMEI"],["id":"2","nombre":"Señal OSIPTEL","imagen":"ic_senal_osiptel"],["id":"4","nombre":"Expedientes Trasu","imagen":"ic_expedientes"],["id":"5","nombre":"Guía de Información","imagen":"ic_guia_informativa"],["id":"6","nombre":"Verifica tu línea","imagen":"ic_verifica_linea"],["id":"7","nombre":"Nuestras oficinas","imagen":"ic_nuestras"],["id":"8","nombre":"Comparatel","imagen":"ic_comptel"],["id":"9","nombre":"Comparamóvil","imagen":"ic_comparamovil"],["id":"10","nombre":"Punku","imagen":"ic_punku"],["id":"11","nombre":"Califícanos","imagen":"ic_calificanos"]] as [[String:AnyObject]]

                //Opciones.append(["id":"8","nombre":"Califícanos","imagen":"ic_calificanos"] as! [String : AnyObject])
                rootviewcontroller()
            }
            else{
                rootviewcontroller()
            }
        }else{
                Opciones = [["id":"1","nombre":"Consulta IMEI","imagen":"ic_consulta_IMEI"],["id":"2","nombre":"Señal OSIPTEL","imagen":"ic_senal_osiptel"],["id":"4","nombre":"Expedientes Trasu","imagen":"ic_expedientes"],["id":"5","nombre":"Guía de Información","imagen":"ic_guia_informativa"],["id":"6","nombre":"Verifica tu línea","imagen":"ic_verifica_linea"],["id":"7","nombre":"Nuestras oficinas","imagen":"ic_nuestras"],["id":"8","nombre":"Comparatel","imagen":"ic_comptel"],["id":"9","nombre":"Comparamóvil","imagen":"ic_comparamovil"],["id":"10","nombre":"Punku","imagen":"ic_punku"]] as [[String:AnyObject]]
            UserDefaults.standard.setOpciones(value: Opciones)
            rootviewcontroller()
        }

        UserDefaults.standard.setOpciones(value: Opciones)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Barra de estado desaparece - al cambiar la vista
        UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
    }

    override func viewWillDisappear(_ animated: Bool) {
        //Barra de estado aparece - al cambiar la vista
        UIApplication.shared.setStatusBarHidden(false, with: UIStatusBarAnimation.none)
    }
    
    func attempFetchCreateEventoLog(withId post: BodyEntityLog){
        
        DataService.shared.requestFetchCreateEventoLog(with: post) {[weak self] (trasu, error, String) in
            if error != nil {
                print("error")
                print(error)
            }
            
            if trasu == "Ok"{
                print("CreateEventoLog OK")
            }else if trasu == "error"{
               print("CreateEventoLog ERROR")
            }
        }
    }
    
}

